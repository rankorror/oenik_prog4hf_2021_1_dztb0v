var interface_album_of_the_year_1_1_logic_1_1_i_customer_logic =
[
    [ "AddCustomer", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html#a6c2cdab96517abf8270d2568d171412d", null ],
    [ "GetAllCustomers", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html#ab1a725dd2edee7dc376420748c2ffced", null ],
    [ "GetCustomerById", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html#aa184a8225b7e50401b2a9f87e201b07c", null ],
    [ "RemoveCustomer", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html#afaca8b341c6ce917a0c290de0efad56e", null ],
    [ "UpdateCustomer", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html#ad3ffde0b9d0a5aed9f81b75e13008795", null ]
];