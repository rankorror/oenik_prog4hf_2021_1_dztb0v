var class_album_of_the_year_1_1_logic_1_1_purchaser_logic =
[
    [ "PurchaserLogic", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a0a189acad0fe3247f04b9348494ba696", null ],
    [ "AddCustomer", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a2373691dbade94f24a26c0bb69a8a2da", null ],
    [ "AddOrder", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a45c04aee8b79f36923806aaf99e13c4a", null ],
    [ "GetAllCustomers", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a0d2f036b450b830c9849c5bbf3f5adbc", null ],
    [ "GetAllOrders", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a287a77ed245426af5a443ddb7f034715", null ],
    [ "GetCustomerById", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a957bd6911c1161ab57d2dc17047d846d", null ],
    [ "GetOrderById", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#abf91c98fb82b804fcc57e6123368de7b", null ],
    [ "GetStockList", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a4fc1ee30e305013e4893c9accb45f2cf", null ],
    [ "OrderUpdate", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#afd9417d3f8f609a3cb655f2ac8916dc9", null ],
    [ "RemoveCustomer", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#ad9c3bbe2a7908ce1b194eff55e4d641c", null ],
    [ "RemoveOrder", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a9e5ef0700345f6acb061817af11070c0", null ],
    [ "UpdateCustomer", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a96ad40b21aa12f4f86974f1053f47828", null ],
    [ "IFP", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#a3d80d07bce7075cf3b336cd86f8331d9", null ]
];