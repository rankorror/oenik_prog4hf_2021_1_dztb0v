var class_album_of_the_year_1_1_data_1_1_customer =
[
    [ "Customer", "class_album_of_the_year_1_1_data_1_1_customer.html#ada818ee3f5d896e1e4d769d797ba6c23", null ],
    [ "MainData", "class_album_of_the_year_1_1_data_1_1_customer.html#a6ecbb83c1acf77c4567b40105c175c8a", null ],
    [ "Address", "class_album_of_the_year_1_1_data_1_1_customer.html#a8f24a1d32b672d169233599de3e2ec69", null ],
    [ "Birthdate", "class_album_of_the_year_1_1_data_1_1_customer.html#a01493e0f914e80fd429f4c17218142dd", null ],
    [ "CustomerId", "class_album_of_the_year_1_1_data_1_1_customer.html#af4a9a9cf440221183b7e32b502b971de", null ],
    [ "Email", "class_album_of_the_year_1_1_data_1_1_customer.html#a02a3d41a82e4ed76a543f692d2a7df26", null ],
    [ "Name", "class_album_of_the_year_1_1_data_1_1_customer.html#aedd3afd36d48bf0c6551707e00d47df0", null ],
    [ "Orders", "class_album_of_the_year_1_1_data_1_1_customer.html#ae7b6d58c28b91ba40651505bd2892468", null ],
    [ "PhoneNumber", "class_album_of_the_year_1_1_data_1_1_customer.html#a2aca888e324480843b767b44d354ef37", null ]
];