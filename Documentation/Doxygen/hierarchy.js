var hierarchy =
[
    [ "AlbumOfTheYear.Data.Album", "class_album_of_the_year_1_1_data_1_1_album.html", null ],
    [ "AlbumOfTheYear.Logic.Results.AlbumInShopsResults", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html", null ],
    [ "Application", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.App", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_app.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.Tests.CheckerTests", "class_album_of_the_year_1_1_logic_1_1_tests_1_1_checker_tests.html", null ],
    [ "AlbumOfTheYear.Data.Customer", "class_album_of_the_year_1_1_data_1_1_customer.html", null ],
    [ "AlbumOfTheYear.Logic.Results.CustomerSumResults", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html", null ],
    [ "DbContext", null, [
      [ "AlbumOfTheYear.Data.DbContext", "class_album_of_the_year_1_1_data_1_1_db_context.html", null ]
    ] ],
    [ "AlbumOfTheYear.Program.Factory", "class_album_of_the_year_1_1_program_1_1_factory.html", null ],
    [ "AlbumOfTheYear.WpfAppForCrud.Factory", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_factory.html", null ],
    [ "AlbumOfTheYear.WpfAppForCrud.BL.IAlbumLogic", "interface_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_i_album_logic.html", [
      [ "AlbumOfTheYear.WpfAppForCrud.BL.AlbumLogic", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.IAlbumLogic", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html", [
      [ "AlbumOfTheYear.Logic.ManagementLogic", "class_album_of_the_year_1_1_logic_1_1_management_logic.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.UI.EditorWindow", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.ICustomerLogic", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html", [
      [ "AlbumOfTheYear.Logic.PurchaserLogic", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.WpfAppForCrud.BL.IEditorService", "interface_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_i_editor_service.html", [
      [ "AlbumOfTheYear.WpfAppForCrud.UI.EditorServiceViaWindow", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.IOrderLogic", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html", [
      [ "AlbumOfTheYear.Logic.PurchaserLogic", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< T >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.Repo< T >", "class_album_of_the_year_1_1_repository_1_1_repo.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< Album >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.IAlbumRepository", "interface_album_of_the_year_1_1_repository_1_1_i_album_repository.html", [
        [ "AlbumOfTheYear.Repository.AlbumRepository", "class_album_of_the_year_1_1_repository_1_1_album_repository.html", null ]
      ] ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< Customer >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.ICustomerRepository", "interface_album_of_the_year_1_1_repository_1_1_i_customer_repository.html", [
        [ "AlbumOfTheYear.Repository.CustomerRepository", "class_album_of_the_year_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< Order >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.IOrderRepository", "interface_album_of_the_year_1_1_repository_1_1_i_order_repository.html", [
        [ "AlbumOfTheYear.Repository.OrderRepository", "class_album_of_the_year_1_1_repository_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< Shop >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.IShopRepository", "interface_album_of_the_year_1_1_repository_1_1_i_shop_repository.html", [
        [ "AlbumOfTheYear.Repository.ShopRepository", "class_album_of_the_year_1_1_repository_1_1_shop_repository.html", null ]
      ] ]
    ] ],
    [ "AlbumOfTheYear.Repository.IRepository< Stock >", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", [
      [ "AlbumOfTheYear.Repository.IStockRepository", "interface_album_of_the_year_1_1_repository_1_1_i_stock_repository.html", [
        [ "AlbumOfTheYear.Repository.StockRepository", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.MyIoc", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_my_ioc.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.IShopLogic", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html", [
      [ "AlbumOfTheYear.Logic.ManagementLogic", "class_album_of_the_year_1_1_logic_1_1_management_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.Interfaces.IStockList", "interface_album_of_the_year_1_1_logic_1_1_interfaces_1_1_i_stock_list.html", [
      [ "AlbumOfTheYear.Logic.PurchaserLogic", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.IStockLogic", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html", [
      [ "AlbumOfTheYear.Logic.ManagementLogic", "class_album_of_the_year_1_1_logic_1_1_management_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.Interfaces.ISumChecker", "interface_album_of_the_year_1_1_logic_1_1_interfaces_1_1_i_sum_checker.html", [
      [ "AlbumOfTheYear.Logic.CheckerLogic", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html", null ]
    ] ],
    [ "AlbumOfTheYear.Logic.Tests.ManagementTests", "class_album_of_the_year_1_1_logic_1_1_tests_1_1_management_tests.html", null ],
    [ "AlbumOfTheYear.Program.Menu", "class_album_of_the_year_1_1_program_1_1_menu.html", null ],
    [ "ObservableObject", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.Data.AlbumCopy", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html", null ]
    ] ],
    [ "AlbumOfTheYear.Data.Model.Order", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html", null ],
    [ "AlbumOfTheYear.Program.Program", "class_album_of_the_year_1_1_program_1_1_program.html", null ],
    [ "AlbumOfTheYear.Logic.Tests.PurchaserTests", "class_album_of_the_year_1_1_logic_1_1_tests_1_1_purchaser_tests.html", null ],
    [ "AlbumOfTheYear.Repository.Repo< Album >", "class_album_of_the_year_1_1_repository_1_1_repo.html", [
      [ "AlbumOfTheYear.Repository.AlbumRepository", "class_album_of_the_year_1_1_repository_1_1_album_repository.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.Repo< Customer >", "class_album_of_the_year_1_1_repository_1_1_repo.html", [
      [ "AlbumOfTheYear.Repository.CustomerRepository", "class_album_of_the_year_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.Repo< Order >", "class_album_of_the_year_1_1_repository_1_1_repo.html", [
      [ "AlbumOfTheYear.Repository.OrderRepository", "class_album_of_the_year_1_1_repository_1_1_order_repository.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.Repo< Shop >", "class_album_of_the_year_1_1_repository_1_1_repo.html", [
      [ "AlbumOfTheYear.Repository.ShopRepository", "class_album_of_the_year_1_1_repository_1_1_shop_repository.html", null ]
    ] ],
    [ "AlbumOfTheYear.Repository.Repo< Stock >", "class_album_of_the_year_1_1_repository_1_1_repo.html", [
      [ "AlbumOfTheYear.Repository.StockRepository", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html", null ]
    ] ],
    [ "AlbumOfTheYear.Data.Shop", "class_album_of_the_year_1_1_data_1_1_shop.html", null ],
    [ "SimpleIoc", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.MyIoc", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_my_ioc.html", null ]
    ] ],
    [ "AlbumOfTheYear.Data.Stock", "class_album_of_the_year_1_1_data_1_1_stock.html", null ],
    [ "AlbumOfTheYear.Logic.Results.StockListResults", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html", null ],
    [ "AlbumOfTheYear.Logic.StockSumResults", "class_album_of_the_year_1_1_logic_1_1_stock_sum_results.html", null ],
    [ "ViewModelBase", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.VM.EditorViewModel", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "AlbumOfTheYear.WpfAppForCrud.VM.MainViewModel", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.UI.EditorWindow", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "AlbumOfTheYear.WpfAppForCrud.MainWindow", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_main_window.html", null ]
    ] ]
];