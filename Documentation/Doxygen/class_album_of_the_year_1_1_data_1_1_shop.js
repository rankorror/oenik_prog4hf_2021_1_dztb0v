var class_album_of_the_year_1_1_data_1_1_shop =
[
    [ "Shop", "class_album_of_the_year_1_1_data_1_1_shop.html#a59dcbcb21172029c94bf1fe53ded88c5", null ],
    [ "MainData", "class_album_of_the_year_1_1_data_1_1_shop.html#a664bc6beda2fa828824d8263575cee9b", null ],
    [ "ToString", "class_album_of_the_year_1_1_data_1_1_shop.html#a1d1c6d1a7ac300e649810ec5ec15905d", null ],
    [ "City", "class_album_of_the_year_1_1_data_1_1_shop.html#a54e9495716b146b6426a5ffe9bf9a07c", null ],
    [ "ManagerName", "class_album_of_the_year_1_1_data_1_1_shop.html#ac36b3f6e1fed1a24f369584660c3aeb5", null ],
    [ "OpeningDate", "class_album_of_the_year_1_1_data_1_1_shop.html#a0db2a4c20bae8e6cb2110e5bb883e987", null ],
    [ "Orders", "class_album_of_the_year_1_1_data_1_1_shop.html#a6f4573f473de397b979a34a3daf27fd4", null ],
    [ "ShopId", "class_album_of_the_year_1_1_data_1_1_shop.html#a75a7c6219b037296378448add5eedde5", null ],
    [ "ShopName", "class_album_of_the_year_1_1_data_1_1_shop.html#a53477e825b0629f7482d325f9c2a77fa", null ],
    [ "Stocks", "class_album_of_the_year_1_1_data_1_1_shop.html#aa2b5965496131c6091e6425f498a081d", null ]
];