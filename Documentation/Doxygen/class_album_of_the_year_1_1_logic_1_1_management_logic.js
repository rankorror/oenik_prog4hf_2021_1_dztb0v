var class_album_of_the_year_1_1_logic_1_1_management_logic =
[
    [ "ManagementLogic", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#aabee539f17fc44fb5f1cd2471112628d", null ],
    [ "AddAlbum", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a9f8e7ccd0700160b95c5e5d4b6e5af55", null ],
    [ "AddShop", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#aee17f2f24f2bb099b0423077047cbd08", null ],
    [ "AddStock", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a33a42ac1083cd79768720d47c114036b", null ],
    [ "GetAlbumById", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a542a7f8d1477e4db491bad11a60e9c88", null ],
    [ "GetAllAlbums", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#aad992880dc1649b059d312771e8e59e7", null ],
    [ "GetAllShops", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a8f239a77e17bc62ca8274cfd1f65cff8", null ],
    [ "GetAllStocks", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a73f52fcac61c0f4c2036813e827c8ad8", null ],
    [ "GetShopById", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a82dba58072286e0dfb85750d84b076b7", null ],
    [ "GetStockByIds", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#aff14ea11c4c555ad8fbeb48cc51f59bb", null ],
    [ "RemoveAlbum", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a20ea37ffe6cff0e08521e0b2ebf23814", null ],
    [ "RemoveShop", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#ab7cbc398c57b1f9d5650ca2d34337573", null ],
    [ "RemoveStock", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a9b3bc7f5bbf6c3eee95cee0be2fe854b", null ],
    [ "ShopUpdate", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#ae5806f8916f13d796d6f542a96f177a7", null ],
    [ "StockUpdate", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a231933cc28e9baf9fad9e1c691cacc05", null ],
    [ "UpdateAlbum", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a3e1d4dd4a6dd198a0071de4c54dd9d8f", null ],
    [ "IFP", "class_album_of_the_year_1_1_logic_1_1_management_logic.html#a72b2dd98bb44a0339f4e3aab58ae1383", null ]
];