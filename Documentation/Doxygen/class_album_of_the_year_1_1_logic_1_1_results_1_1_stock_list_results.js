var class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results =
[
    [ "Equals", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a3c4d323e05849ef1cd7781b0e81dbbe8", null ],
    [ "GetHashCode", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#ad2bd72d3231fbc39d1bb01f204ff39f3", null ],
    [ "ToString", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a58ea86ffb514772ba8575d153d991111", null ],
    [ "AlbumId", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#ae421f99f6b61678234fbd3567741befe", null ],
    [ "AlbumName", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a5070bbb989bf0c37691555052358822f", null ],
    [ "Number", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a57a0d6d74d448a1dd3b9e59939712edf", null ],
    [ "Performer", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a120013b67d73d2ca15a5a15848632be4", null ],
    [ "Reissue", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#abc08efae8644123aa21e525f931300af", null ],
    [ "ShopId", "class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#ae7bc0f61a76bb8c1117292db2f529053", null ]
];