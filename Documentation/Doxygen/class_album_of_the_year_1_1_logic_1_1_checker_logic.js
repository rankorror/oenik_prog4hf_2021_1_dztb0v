var class_album_of_the_year_1_1_logic_1_1_checker_logic =
[
    [ "CheckerLogic", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#ac7082d045201e6792af1c713c4dfdb6d", null ],
    [ "GetAlbumInShopSums", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#a0d07b864736fd0a5c9a1381bc2c29a89", null ],
    [ "GetAlbumInShopSumsAsync", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#af5607a20ac62de2fe5060e869abd70a6", null ],
    [ "GetCustomerSums", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#a8719af038bf5030751f342feb2f3d65e", null ],
    [ "GetCustomerSumsAsync", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#a06e00fe6cc002be4217beca6ef246daf", null ],
    [ "GetStockSums", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#a4082b3ee8bfda9040a1f4eabba18fc54", null ],
    [ "GetStockSumsAsync", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html#a93020b4daca07af7be57d6ed8b5ddf70", null ]
];