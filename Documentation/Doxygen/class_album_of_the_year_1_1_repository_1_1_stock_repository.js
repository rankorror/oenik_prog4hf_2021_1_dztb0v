var class_album_of_the_year_1_1_repository_1_1_stock_repository =
[
    [ "StockRepository", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#ab2d1c01cbfb4e84d85afcf8b993060c4", null ],
    [ "GetOne", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#adf4186c3ef3cdfdcf53a7ea61157b1ce", null ],
    [ "GetOne", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#a68e5a398e7775edc3b395de8444f6931", null ],
    [ "Insert", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#a75a6b2009745f895412896fbcf1eb188", null ],
    [ "Remove", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#a09087bfa9baf9fcba0bd4851a672fdbd", null ],
    [ "Remove", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#a87b817bc5fe2d2c9b665a5a786b36911", null ],
    [ "UpdateStock", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html#ae7e672e1fd603dc5bb5b3b37610e546b", null ]
];