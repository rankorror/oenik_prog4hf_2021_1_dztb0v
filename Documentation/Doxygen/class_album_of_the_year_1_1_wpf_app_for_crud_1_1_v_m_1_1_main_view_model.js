var class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a7bd7f073116da4d09a82f0362e234c1d", null ],
    [ "MainViewModel", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#ad50893f861076b7a88db18f6901dde02", null ],
    [ "AddCmd", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a7452de2e95ff931cfe3051fa6d9f9820", null ],
    [ "Albums", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a8889eb0fa62cde98d06ff5fb1ab510c0", null ],
    [ "AlbumSelected", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a52359a6669828d4e38de92174f2b9cd3", null ],
    [ "DelCmd", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a27a548a5d659b47dee430b919fd659e5", null ],
    [ "ModCmd", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m_1_1_main_view_model.html#a21ba981e2ceabcad42ed15c5e754ecae", null ]
];