var searchData=
[
  ['name_391',['Name',['../class_album_of_the_year_1_1_data_1_1_customer.html#aedd3afd36d48bf0c6551707e00d47df0',1,'AlbumOfTheYear::Data::Customer']]],
  ['number_392',['Number',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_stock_list_results.html#a57a0d6d74d448a1dd3b9e59939712edf',1,'AlbumOfTheYear::Logic::Results::StockListResults']]],
  ['numberofappearances_393',['NumberOfAppearances',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a6d4cd212a9ab4d80b6a5c0756c3a34f3',1,'AlbumOfTheYear::Logic::Results::AlbumInShopsResults']]],
  ['numberoforders_394',['NumberOfOrders',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#ab0dbe6b836a259b6eec40d023efcb8ea',1,'AlbumOfTheYear::Logic::Results::CustomerSumResults']]],
  ['numberofpieces_395',['NumberOfPieces',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a6abd48cfeb7578ccad264ccc425196af',1,'AlbumOfTheYear::Logic::Results::AlbumInShopsResults']]]
];
