var searchData=
[
  ['chckr_371',['CHCKR',['../class_album_of_the_year_1_1_program_1_1_factory.html#a76d3ab59305c2e8c4d46e27a424b0ade',1,'AlbumOfTheYear.Program.Factory.CHCKR()'],['../class_album_of_the_year_1_1_wpf_app_for_crud_1_1_factory.html#a8828ed4625a8cc244e2ff829c406d5c5',1,'AlbumOfTheYear.WpfAppForCrud.Factory.CHCKR()']]],
  ['checkerl_372',['CheckerL',['../class_album_of_the_year_1_1_program_1_1_menu.html#af4bbfb8b47d45ace345a79e6f372225a',1,'AlbumOfTheYear::Program::Menu']]],
  ['city_373',['City',['../class_album_of_the_year_1_1_data_1_1_shop.html#a54e9495716b146b6426a5ffe9bf9a07c',1,'AlbumOfTheYear::Data::Shop']]],
  ['cm_374',['Cm',['../class_album_of_the_year_1_1_program_1_1_menu.html#a8d4caf2414ca98b6e86ce412fd005c28',1,'AlbumOfTheYear::Program::Menu']]],
  ['ctx_375',['Ctx',['../class_album_of_the_year_1_1_repository_1_1_repo.html#a9f589b810c669c1880d699016e87ff73',1,'AlbumOfTheYear::Repository::Repo']]],
  ['customer_376',['Customer',['../class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#a19b84d307b419911a3c93174b46627a1',1,'AlbumOfTheYear::Data::Model::Order']]],
  ['customerid_377',['CustomerId',['../class_album_of_the_year_1_1_data_1_1_customer.html#af4a9a9cf440221183b7e32b502b971de',1,'AlbumOfTheYear.Data.Customer.CustomerId()'],['../class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#af6be9d0a6c9023ab07a65ab0aa162ec2',1,'AlbumOfTheYear.Data.Model.Order.CustomerId()'],['../class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a1c5efc84508ab4a169a429021a281c04',1,'AlbumOfTheYear.Logic.Results.CustomerSumResults.CustomerId()']]],
  ['customername_378',['CustomerName',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a8be214a3e0f4eaa3425ac671decaaca0',1,'AlbumOfTheYear::Logic::Results::CustomerSumResults']]],
  ['customers_379',['Customers',['../class_album_of_the_year_1_1_data_1_1_db_context.html#a698de2d89a8592f0611424b9d8a814b3',1,'AlbumOfTheYear::Data::DbContext']]]
];
