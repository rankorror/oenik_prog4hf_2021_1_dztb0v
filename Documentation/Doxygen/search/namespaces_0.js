var searchData=
[
  ['albumoftheyear_254',['AlbumOfTheYear',['../namespace_album_of_the_year.html',1,'']]],
  ['bl_255',['BL',['../namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l.html',1,'AlbumOfTheYear::WpfAppForCrud']]],
  ['data_256',['Data',['../namespace_album_of_the_year_1_1_data.html',1,'AlbumOfTheYear.Data'],['../namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_data.html',1,'AlbumOfTheYear.WpfAppForCrud.Data']]],
  ['interfaces_257',['Interfaces',['../namespace_album_of_the_year_1_1_logic_1_1_interfaces.html',1,'AlbumOfTheYear::Logic']]],
  ['logic_258',['Logic',['../namespace_album_of_the_year_1_1_logic.html',1,'AlbumOfTheYear']]],
  ['model_259',['Model',['../namespace_album_of_the_year_1_1_data_1_1_model.html',1,'AlbumOfTheYear::Data']]],
  ['program_260',['Program',['../namespace_album_of_the_year_1_1_program.html',1,'AlbumOfTheYear']]],
  ['repository_261',['Repository',['../namespace_album_of_the_year_1_1_repository.html',1,'AlbumOfTheYear']]],
  ['results_262',['Results',['../namespace_album_of_the_year_1_1_logic_1_1_results.html',1,'AlbumOfTheYear::Logic']]],
  ['tests_263',['Tests',['../namespace_album_of_the_year_1_1_logic_1_1_tests.html',1,'AlbumOfTheYear::Logic']]],
  ['ui_264',['UI',['../namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i.html',1,'AlbumOfTheYear::WpfAppForCrud']]],
  ['vm_265',['VM',['../namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m.html',1,'AlbumOfTheYear::WpfAppForCrud']]],
  ['wpfappforcrud_266',['WpfAppForCrud',['../namespace_album_of_the_year_1_1_wpf_app_for_crud.html',1,'AlbumOfTheYear']]]
];
