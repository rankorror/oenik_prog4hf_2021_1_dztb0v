var searchData=
[
  ['onconfiguring_326',['OnConfiguring',['../class_album_of_the_year_1_1_data_1_1_db_context.html#a87640d6e01d085943cfcaa9f91a6b9f9',1,'AlbumOfTheYear::Data::DbContext']]],
  ['onmodelcreating_327',['OnModelCreating',['../class_album_of_the_year_1_1_data_1_1_db_context.html#a0649785a0dbad22286302b6c1da94640',1,'AlbumOfTheYear::Data::DbContext']]],
  ['orderrepository_328',['OrderRepository',['../class_album_of_the_year_1_1_repository_1_1_order_repository.html#a821ef2ffac4b886b9c5534490b84a9d8',1,'AlbumOfTheYear::Repository::OrderRepository']]],
  ['orderupdate_329',['OrderUpdate',['../interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#ac8cf698543637ae386c7912840c67395',1,'AlbumOfTheYear.Logic.IOrderLogic.OrderUpdate()'],['../class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html#afd9417d3f8f609a3cb655f2ac8916dc9',1,'AlbumOfTheYear.Logic.PurchaserLogic.OrderUpdate()'],['../interface_album_of_the_year_1_1_repository_1_1_i_order_repository.html#a36f0cfee61bd53b54036bda57da99fc0',1,'AlbumOfTheYear.Repository.IOrderRepository.OrderUpdate()'],['../class_album_of_the_year_1_1_repository_1_1_order_repository.html#a871ffcb8bcc0a84a1cffa4bf26806ed0',1,'AlbumOfTheYear.Repository.OrderRepository.OrderUpdate()']]]
];
