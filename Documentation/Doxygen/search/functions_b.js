var searchData=
[
  ['setpropertyvalue_339',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['setup_340',['Setup',['../class_album_of_the_year_1_1_logic_1_1_tests_1_1_checker_tests.html#ad0625a7345d44c04463366508da106d7',1,'AlbumOfTheYear.Logic.Tests.CheckerTests.Setup()'],['../class_album_of_the_year_1_1_logic_1_1_tests_1_1_management_tests.html#a5c0c0692554b290c19abcb63fe0e5a19',1,'AlbumOfTheYear.Logic.Tests.ManagementTests.Setup()'],['../class_album_of_the_year_1_1_logic_1_1_tests_1_1_purchaser_tests.html#a05f16ff17f4438020d4d4133e22ef6f3',1,'AlbumOfTheYear.Logic.Tests.PurchaserTests.Setup()']]],
  ['shop_341',['Shop',['../class_album_of_the_year_1_1_data_1_1_shop.html#a59dcbcb21172029c94bf1fe53ded88c5',1,'AlbumOfTheYear::Data::Shop']]],
  ['shoprepository_342',['ShopRepository',['../class_album_of_the_year_1_1_repository_1_1_shop_repository.html#ac48bf5f08d9cc63ca985e9cbc1aed49b',1,'AlbumOfTheYear::Repository::ShopRepository']]],
  ['shopupdate_343',['ShopUpdate',['../interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#a88ab03234ac7301f552bc6f915622701',1,'AlbumOfTheYear.Logic.IShopLogic.ShopUpdate()'],['../class_album_of_the_year_1_1_logic_1_1_management_logic.html#ae5806f8916f13d796d6f542a96f177a7',1,'AlbumOfTheYear.Logic.ManagementLogic.ShopUpdate()']]],
  ['showsums_344',['ShowSums',['../class_album_of_the_year_1_1_program_1_1_menu.html#a5e880c0196f2561e10a794dce5d795cc',1,'AlbumOfTheYear::Program::Menu']]],
  ['stocklisting_345',['StockListing',['../class_album_of_the_year_1_1_program_1_1_menu.html#a330657db969696ec5406f502c8a15343',1,'AlbumOfTheYear::Program::Menu']]],
  ['stockrepository_346',['StockRepository',['../class_album_of_the_year_1_1_repository_1_1_stock_repository.html#ab2d1c01cbfb4e84d85afcf8b993060c4',1,'AlbumOfTheYear::Repository::StockRepository']]],
  ['stockupdate_347',['StockUpdate',['../interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#a0825dc79d72910dd556ec57f24046c7c',1,'AlbumOfTheYear.Logic.IStockLogic.StockUpdate()'],['../class_album_of_the_year_1_1_logic_1_1_management_logic.html#a231933cc28e9baf9fad9e1c691cacc05',1,'AlbumOfTheYear.Logic.ManagementLogic.StockUpdate()']]]
];
