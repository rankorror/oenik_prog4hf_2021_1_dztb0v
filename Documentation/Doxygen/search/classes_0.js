var searchData=
[
  ['album_195',['Album',['../class_album_of_the_year_1_1_data_1_1_album.html',1,'AlbumOfTheYear::Data']]],
  ['albumcopy_196',['AlbumCopy',['../class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html',1,'AlbumOfTheYear::WpfAppForCrud::Data']]],
  ['albuminshopsresults_197',['AlbumInShopsResults',['../class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html',1,'AlbumOfTheYear::Logic::Results']]],
  ['albumlogic_198',['AlbumLogic',['../class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html',1,'AlbumOfTheYear::WpfAppForCrud::BL']]],
  ['albumrepository_199',['AlbumRepository',['../class_album_of_the_year_1_1_repository_1_1_album_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['app_200',['App',['../class_album_of_the_year_1_1_wpf_app_for_crud_1_1_app.html',1,'AlbumOfTheYear::WpfAppForCrud']]]
];
