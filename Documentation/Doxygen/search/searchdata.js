var indexSectionsWithContent =
{
  0: "abcdefgilmnopqrstux",
  1: "acdefgimoprs",
  2: "ax",
  3: "acdegilmoprstu",
  4: "abcdefgimnopqrst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties"
};

