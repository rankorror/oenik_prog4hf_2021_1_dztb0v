var searchData=
[
  ['ialbumlogic_212',['IAlbumLogic',['../interface_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_i_album_logic.html',1,'AlbumOfTheYear.WpfAppForCrud.BL.IAlbumLogic'],['../interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html',1,'AlbumOfTheYear.Logic.IAlbumLogic']]],
  ['ialbumrepository_213',['IAlbumRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_album_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['icustomerlogic_214',['ICustomerLogic',['../interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html',1,'AlbumOfTheYear::Logic']]],
  ['icustomerrepository_215',['ICustomerRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_customer_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['ieditorservice_216',['IEditorService',['../interface_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_i_editor_service.html',1,'AlbumOfTheYear::WpfAppForCrud::BL']]],
  ['iorderlogic_217',['IOrderLogic',['../interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html',1,'AlbumOfTheYear::Logic']]],
  ['iorderrepository_218',['IOrderRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_order_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_219',['IRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_3c_20album_20_3e_220',['IRepository&lt; Album &gt;',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_3c_20customer_20_3e_221',['IRepository&lt; Customer &gt;',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_3c_20order_20_3e_222',['IRepository&lt; Order &gt;',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_3c_20shop_20_3e_223',['IRepository&lt; Shop &gt;',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['irepository_3c_20stock_20_3e_224',['IRepository&lt; Stock &gt;',['../interface_album_of_the_year_1_1_repository_1_1_i_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['ishoplogic_225',['IShopLogic',['../interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html',1,'AlbumOfTheYear::Logic']]],
  ['ishoprepository_226',['IShopRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_shop_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['istocklist_227',['IStockList',['../interface_album_of_the_year_1_1_logic_1_1_interfaces_1_1_i_stock_list.html',1,'AlbumOfTheYear::Logic::Interfaces']]],
  ['istocklogic_228',['IStockLogic',['../interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html',1,'AlbumOfTheYear::Logic']]],
  ['istockrepository_229',['IStockRepository',['../interface_album_of_the_year_1_1_repository_1_1_i_stock_repository.html',1,'AlbumOfTheYear::Repository']]],
  ['isumchecker_230',['ISumChecker',['../interface_album_of_the_year_1_1_logic_1_1_interfaces_1_1_i_sum_checker.html',1,'AlbumOfTheYear::Logic::Interfaces']]]
];
