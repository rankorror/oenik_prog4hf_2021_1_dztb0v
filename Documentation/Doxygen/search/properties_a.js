var searchData=
[
  ['openingdate_396',['OpeningDate',['../class_album_of_the_year_1_1_data_1_1_shop.html#a0db2a4c20bae8e6cb2110e5bb883e987',1,'AlbumOfTheYear::Data::Shop']]],
  ['orderdate_397',['OrderDate',['../class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#ae8475ad2753f1dea11ea6ba72623cbc7',1,'AlbumOfTheYear::Data::Model::Order']]],
  ['orderid_398',['OrderId',['../class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#ad5112e5ad03724ede917bea0a44f3e89',1,'AlbumOfTheYear::Data::Model::Order']]],
  ['orders_399',['Orders',['../class_album_of_the_year_1_1_data_1_1_album.html#a406cce60ed8a1c47bcccc5a70155853f',1,'AlbumOfTheYear.Data.Album.Orders()'],['../class_album_of_the_year_1_1_data_1_1_customer.html#ae7b6d58c28b91ba40651505bd2892468',1,'AlbumOfTheYear.Data.Customer.Orders()'],['../class_album_of_the_year_1_1_data_1_1_db_context.html#ae883856017a03af85090ece64417431a',1,'AlbumOfTheYear.Data.DbContext.Orders()'],['../class_album_of_the_year_1_1_data_1_1_shop.html#a6f4573f473de397b979a34a3daf27fd4',1,'AlbumOfTheYear.Data.Shop.Orders()']]]
];
