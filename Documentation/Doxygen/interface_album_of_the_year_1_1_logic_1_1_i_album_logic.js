var interface_album_of_the_year_1_1_logic_1_1_i_album_logic =
[
    [ "AddAlbum", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html#adf26ba282b0f6f10d36cb3cc569c8d02", null ],
    [ "GetAlbumById", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html#a4fd4e2d142d7ac2d470a6b28bed5a8da", null ],
    [ "GetAllAlbums", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html#a261a40fdc9d801e0f67e6f8bc8e77669", null ],
    [ "RemoveAlbum", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html#a5cd7eb351fd40ecc50095883889363ba", null ],
    [ "UpdateAlbum", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html#a889f5b822e33fda6d2d4b6be69fecdb8", null ]
];