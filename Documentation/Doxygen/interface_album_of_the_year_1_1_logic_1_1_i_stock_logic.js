var interface_album_of_the_year_1_1_logic_1_1_i_stock_logic =
[
    [ "AddStock", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#af0240678de26bcb0e43407e83a8c634b", null ],
    [ "GetAllStocks", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#ab351d0137088e465713a2c8e69e80182", null ],
    [ "GetStockByIds", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#a3e0bdcd4666c10e730eb33919f334b2e", null ],
    [ "RemoveStock", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#a862294173f7f164f15fe33a6b6e0b8f6", null ],
    [ "StockUpdate", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html#a0825dc79d72910dd556ec57f24046c7c", null ]
];