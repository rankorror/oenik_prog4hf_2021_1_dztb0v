var class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results =
[
    [ "Equals", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a514d80f4376ff5982c7fde40ae756d91", null ],
    [ "GetHashCode", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a5a8eb0e0d9e89b53147b80e8907ee14b", null ],
    [ "ToString", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a1c96958ac7dd3aaa41fd10d3da07e100", null ],
    [ "CustomerId", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a1c5efc84508ab4a169a429021a281c04", null ],
    [ "CustomerName", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#a8be214a3e0f4eaa3425ac671decaaca0", null ],
    [ "NumberOfOrders", "class_album_of_the_year_1_1_logic_1_1_results_1_1_customer_sum_results.html#ab0dbe6b836a259b6eec40d023efcb8ea", null ]
];