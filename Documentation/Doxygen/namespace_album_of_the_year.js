var namespace_album_of_the_year =
[
    [ "Data", "namespace_album_of_the_year_1_1_data.html", "namespace_album_of_the_year_1_1_data" ],
    [ "Logic", "namespace_album_of_the_year_1_1_logic.html", "namespace_album_of_the_year_1_1_logic" ],
    [ "Program", "namespace_album_of_the_year_1_1_program.html", "namespace_album_of_the_year_1_1_program" ],
    [ "Repository", "namespace_album_of_the_year_1_1_repository.html", "namespace_album_of_the_year_1_1_repository" ],
    [ "WpfAppForCrud", "namespace_album_of_the_year_1_1_wpf_app_for_crud.html", "namespace_album_of_the_year_1_1_wpf_app_for_crud" ]
];