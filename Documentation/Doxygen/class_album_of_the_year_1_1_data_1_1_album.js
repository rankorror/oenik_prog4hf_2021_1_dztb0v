var class_album_of_the_year_1_1_data_1_1_album =
[
    [ "Album", "class_album_of_the_year_1_1_data_1_1_album.html#a5a6852f973b2999020627aba7349140d", null ],
    [ "MainData", "class_album_of_the_year_1_1_data_1_1_album.html#a8f844c5d667500fa3c19bd4c36022bb1", null ],
    [ "AlbumId", "class_album_of_the_year_1_1_data_1_1_album.html#ad12474cc9dae156fda601441903bcf5e", null ],
    [ "Genre", "class_album_of_the_year_1_1_data_1_1_album.html#a336eb6b6617bdd3c8ee05193c81355b2", null ],
    [ "IsReissue", "class_album_of_the_year_1_1_data_1_1_album.html#ac64a61d3143629a475b3591a1dae31cd", null ],
    [ "Orders", "class_album_of_the_year_1_1_data_1_1_album.html#a406cce60ed8a1c47bcccc5a70155853f", null ],
    [ "Performer", "class_album_of_the_year_1_1_data_1_1_album.html#aad9ccc238d844d4a4bf411ec35a826c6", null ],
    [ "ReleaseYear", "class_album_of_the_year_1_1_data_1_1_album.html#ad0a872e30ece4b4a3fae7891c5244c8d", null ],
    [ "Stocks", "class_album_of_the_year_1_1_data_1_1_album.html#a350dd37112673bb89bb5c19ec801c61e", null ],
    [ "Title", "class_album_of_the_year_1_1_data_1_1_album.html#a10e836c6c9345bead45030bcf037e489", null ]
];