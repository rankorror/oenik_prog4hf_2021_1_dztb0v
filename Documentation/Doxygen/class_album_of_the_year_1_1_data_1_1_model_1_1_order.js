var class_album_of_the_year_1_1_data_1_1_model_1_1_order =
[
    [ "MainData", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#a8891d041d6cb6a6dfa9e34d38a62c132", null ],
    [ "Album", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#a31dfe2c2c03cee303eb10c1b7356298f", null ],
    [ "AlbumId", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#af1ad6bb23347b51554914df6a6cb9ee1", null ],
    [ "Customer", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#a19b84d307b419911a3c93174b46627a1", null ],
    [ "CustomerId", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#af6be9d0a6c9023ab07a65ab0aa162ec2", null ],
    [ "OrderDate", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#ae8475ad2753f1dea11ea6ba72623cbc7", null ],
    [ "OrderId", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#ad5112e5ad03724ede917bea0a44f3e89", null ],
    [ "Shop", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#aead5ff33b76bd5c7d3330850758a9b9a", null ],
    [ "ShopId", "class_album_of_the_year_1_1_data_1_1_model_1_1_order.html#a70ada4e34808beab7ed8b21f43532a62", null ]
];