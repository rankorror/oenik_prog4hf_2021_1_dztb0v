var namespace_album_of_the_year_1_1_data =
[
    [ "Model", "namespace_album_of_the_year_1_1_data_1_1_model.html", "namespace_album_of_the_year_1_1_data_1_1_model" ],
    [ "Album", "class_album_of_the_year_1_1_data_1_1_album.html", "class_album_of_the_year_1_1_data_1_1_album" ],
    [ "Customer", "class_album_of_the_year_1_1_data_1_1_customer.html", "class_album_of_the_year_1_1_data_1_1_customer" ],
    [ "DbContext", "class_album_of_the_year_1_1_data_1_1_db_context.html", "class_album_of_the_year_1_1_data_1_1_db_context" ],
    [ "Shop", "class_album_of_the_year_1_1_data_1_1_shop.html", "class_album_of_the_year_1_1_data_1_1_shop" ],
    [ "Stock", "class_album_of_the_year_1_1_data_1_1_stock.html", "class_album_of_the_year_1_1_data_1_1_stock" ]
];