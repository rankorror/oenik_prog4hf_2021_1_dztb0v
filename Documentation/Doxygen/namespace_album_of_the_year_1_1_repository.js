var namespace_album_of_the_year_1_1_repository =
[
    [ "AlbumRepository", "class_album_of_the_year_1_1_repository_1_1_album_repository.html", "class_album_of_the_year_1_1_repository_1_1_album_repository" ],
    [ "CustomerRepository", "class_album_of_the_year_1_1_repository_1_1_customer_repository.html", "class_album_of_the_year_1_1_repository_1_1_customer_repository" ],
    [ "IAlbumRepository", "interface_album_of_the_year_1_1_repository_1_1_i_album_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_album_repository" ],
    [ "ICustomerRepository", "interface_album_of_the_year_1_1_repository_1_1_i_customer_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_customer_repository" ],
    [ "IOrderRepository", "interface_album_of_the_year_1_1_repository_1_1_i_order_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_order_repository" ],
    [ "IRepository", "interface_album_of_the_year_1_1_repository_1_1_i_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_repository" ],
    [ "IShopRepository", "interface_album_of_the_year_1_1_repository_1_1_i_shop_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_shop_repository" ],
    [ "IStockRepository", "interface_album_of_the_year_1_1_repository_1_1_i_stock_repository.html", "interface_album_of_the_year_1_1_repository_1_1_i_stock_repository" ],
    [ "OrderRepository", "class_album_of_the_year_1_1_repository_1_1_order_repository.html", "class_album_of_the_year_1_1_repository_1_1_order_repository" ],
    [ "Repo", "class_album_of_the_year_1_1_repository_1_1_repo.html", "class_album_of_the_year_1_1_repository_1_1_repo" ],
    [ "ShopRepository", "class_album_of_the_year_1_1_repository_1_1_shop_repository.html", "class_album_of_the_year_1_1_repository_1_1_shop_repository" ],
    [ "StockRepository", "class_album_of_the_year_1_1_repository_1_1_stock_repository.html", "class_album_of_the_year_1_1_repository_1_1_stock_repository" ]
];