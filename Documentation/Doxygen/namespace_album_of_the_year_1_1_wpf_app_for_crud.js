var namespace_album_of_the_year_1_1_wpf_app_for_crud =
[
    [ "BL", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l.html", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l" ],
    [ "Data", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_data.html", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_data" ],
    [ "UI", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i.html", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_u_i" ],
    [ "VM", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m.html", "namespace_album_of_the_year_1_1_wpf_app_for_crud_1_1_v_m" ],
    [ "App", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_app.html", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_app" ],
    [ "Factory", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_factory.html", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_factory" ],
    [ "MainWindow", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_main_window.html", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_main_window" ],
    [ "MyIoc", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_my_ioc.html", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_my_ioc" ]
];