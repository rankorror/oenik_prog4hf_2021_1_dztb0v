var namespace_album_of_the_year_1_1_logic =
[
    [ "Interfaces", "namespace_album_of_the_year_1_1_logic_1_1_interfaces.html", "namespace_album_of_the_year_1_1_logic_1_1_interfaces" ],
    [ "Results", "namespace_album_of_the_year_1_1_logic_1_1_results.html", "namespace_album_of_the_year_1_1_logic_1_1_results" ],
    [ "Tests", "namespace_album_of_the_year_1_1_logic_1_1_tests.html", "namespace_album_of_the_year_1_1_logic_1_1_tests" ],
    [ "CheckerLogic", "class_album_of_the_year_1_1_logic_1_1_checker_logic.html", "class_album_of_the_year_1_1_logic_1_1_checker_logic" ],
    [ "IAlbumLogic", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic.html", "interface_album_of_the_year_1_1_logic_1_1_i_album_logic" ],
    [ "ICustomerLogic", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic.html", "interface_album_of_the_year_1_1_logic_1_1_i_customer_logic" ],
    [ "IOrderLogic", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic" ],
    [ "IShopLogic", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic" ],
    [ "IStockLogic", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic.html", "interface_album_of_the_year_1_1_logic_1_1_i_stock_logic" ],
    [ "ManagementLogic", "class_album_of_the_year_1_1_logic_1_1_management_logic.html", "class_album_of_the_year_1_1_logic_1_1_management_logic" ],
    [ "PurchaserLogic", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic.html", "class_album_of_the_year_1_1_logic_1_1_purchaser_logic" ],
    [ "StockSumResults", "class_album_of_the_year_1_1_logic_1_1_stock_sum_results.html", "class_album_of_the_year_1_1_logic_1_1_stock_sum_results" ]
];