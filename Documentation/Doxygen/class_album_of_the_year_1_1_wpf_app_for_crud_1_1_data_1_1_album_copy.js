var class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy =
[
    [ "CopyFrom", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#ab4b8c68e32dd536091dd2d9c243df191", null ],
    [ "AlbumId", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#a34112ed835837b8dbd19bb688fa559d3", null ],
    [ "Genre", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#ac25df158ebd7fd1e8aded2a2b10ab1e3", null ],
    [ "IsReissue", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#a50dc26af63e2592568e615a3b9fa6ede", null ],
    [ "Performer", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#a7dcada295887e88ccc967ca29bf0d0fd", null ],
    [ "ReleaseYear", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#abaeeb477a74257680df47e04e57668ba", null ],
    [ "Title", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_data_1_1_album_copy.html#a5b7f77238fd332a5088f8b80755c11ca", null ]
];