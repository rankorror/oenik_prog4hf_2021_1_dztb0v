var class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results =
[
    [ "Equals", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a292ab06009764c92a1fff19828d2c98f", null ],
    [ "GetHashCode", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#aaea945264af8fc9b6add2bcdb41c7f7d", null ],
    [ "ToString", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a5850c18f46826a304d46552ae9e52d4e", null ],
    [ "AlbumId", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a062209543250e489c2063e8b4e6dd0fa", null ],
    [ "AlbumName", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#ad0474b2686ff56dd99465558b3f95962", null ],
    [ "NumberOfAppearances", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a6d4cd212a9ab4d80b6a5c0756c3a34f3", null ],
    [ "NumberOfPieces", "class_album_of_the_year_1_1_logic_1_1_results_1_1_album_in_shops_results.html#a6abd48cfeb7578ccad264ccc425196af", null ]
];