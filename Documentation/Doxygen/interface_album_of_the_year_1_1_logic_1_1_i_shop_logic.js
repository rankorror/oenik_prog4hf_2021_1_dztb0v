var interface_album_of_the_year_1_1_logic_1_1_i_shop_logic =
[
    [ "AddShop", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#ac186da683bee85456a71ab88bd7890b8", null ],
    [ "GetAllShops", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#a8a81d839387d0b3709871062a45fb558", null ],
    [ "GetShopById", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#afb414ce1077af5d4d7451e3d8639fef0", null ],
    [ "RemoveShop", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#a589bb3fac86ad1f6522a098c0841caf3", null ],
    [ "ShopUpdate", "interface_album_of_the_year_1_1_logic_1_1_i_shop_logic.html#a88ab03234ac7301f552bc6f915622701", null ]
];