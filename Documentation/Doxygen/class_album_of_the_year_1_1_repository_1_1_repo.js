var class_album_of_the_year_1_1_repository_1_1_repo =
[
    [ "Repo", "class_album_of_the_year_1_1_repository_1_1_repo.html#a2a231a525f8e6a927fd2e5e4f2154143", null ],
    [ "GetAll", "class_album_of_the_year_1_1_repository_1_1_repo.html#a6c6c2cda08a13af1c09eb166cec0f8c9", null ],
    [ "GetOne", "class_album_of_the_year_1_1_repository_1_1_repo.html#a29ec43ff260784b3dd8bf44455055655", null ],
    [ "GetOne", "class_album_of_the_year_1_1_repository_1_1_repo.html#a074b7970dfc1ac162a22d55b69615c55", null ],
    [ "Insert", "class_album_of_the_year_1_1_repository_1_1_repo.html#a496abb8ec66960101c3f2e104e67a79a", null ],
    [ "Remove", "class_album_of_the_year_1_1_repository_1_1_repo.html#aaee34f8b4e418131c3fdf0c88a829320", null ],
    [ "Ctx", "class_album_of_the_year_1_1_repository_1_1_repo.html#a9f589b810c669c1880d699016e87ff73", null ]
];