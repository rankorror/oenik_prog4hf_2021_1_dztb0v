var interface_album_of_the_year_1_1_logic_1_1_i_order_logic =
[
    [ "AddOrder", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#a5096bf08de99af7ee545d2ec3016ddb3", null ],
    [ "GetAllOrders", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#a3885cd4d270b6b3f82ff2426d4fa995d", null ],
    [ "GetOrderById", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#a989d7bac96058d7bb9c0901c4c123f9b", null ],
    [ "OrderUpdate", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#ac8cf698543637ae386c7912840c67395", null ],
    [ "RemoveOrder", "interface_album_of_the_year_1_1_logic_1_1_i_order_logic.html#a15fb5b99df2e4921664a21d3d8f1352f", null ]
];