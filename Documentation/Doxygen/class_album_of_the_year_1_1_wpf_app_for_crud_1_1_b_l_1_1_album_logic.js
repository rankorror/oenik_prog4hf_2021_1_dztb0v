var class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic =
[
    [ "AlbumLogic", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html#a9d8c78eec68db68e2326e267049f797c", null ],
    [ "AddAlbum", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html#ae3792194323f8b54d053dc3d77d4df6e", null ],
    [ "DelAlbum", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html#a4b5b7cb98f9615405c9f80a51d2e889e", null ],
    [ "GetAllAlbums", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html#ab356e8db488ad954d5b072c1644fcf13", null ],
    [ "ModAlbum", "class_album_of_the_year_1_1_wpf_app_for_crud_1_1_b_l_1_1_album_logic.html#ab0a26ad4599254621153dc104aaff651", null ]
];