var class_album_of_the_year_1_1_data_1_1_db_context =
[
    [ "DbContext", "class_album_of_the_year_1_1_data_1_1_db_context.html#adeafc40aff56dba57277e3cacc860b36", null ],
    [ "OnConfiguring", "class_album_of_the_year_1_1_data_1_1_db_context.html#a87640d6e01d085943cfcaa9f91a6b9f9", null ],
    [ "OnModelCreating", "class_album_of_the_year_1_1_data_1_1_db_context.html#a0649785a0dbad22286302b6c1da94640", null ],
    [ "Albums", "class_album_of_the_year_1_1_data_1_1_db_context.html#a9ab74a407a3e96fccd57eb67b8795dac", null ],
    [ "Customers", "class_album_of_the_year_1_1_data_1_1_db_context.html#a698de2d89a8592f0611424b9d8a814b3", null ],
    [ "Ifp", "class_album_of_the_year_1_1_data_1_1_db_context.html#a87b6e3dd036b9a19fab69a70fbb94c4e", null ],
    [ "Orders", "class_album_of_the_year_1_1_data_1_1_db_context.html#ae883856017a03af85090ece64417431a", null ],
    [ "Shops", "class_album_of_the_year_1_1_data_1_1_db_context.html#a272f8b9e093284fc25d89ff983eaa833", null ],
    [ "Stocks", "class_album_of_the_year_1_1_data_1_1_db_context.html#aa9ee31fc410afd15fa06e5c4c2a6e652", null ]
];