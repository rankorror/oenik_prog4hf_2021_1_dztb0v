﻿// <copyright file="ManagementTests.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Data.Model;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the class: ManagementLogic.
    /// </summary>
    [TestFixture]
    public class ManagementTests
    {
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<IShopRepository> mockedShopRepo;
        private Mock<IStockRepository> mockedStockRepo;

        private ManagementLogic mLogic;

        private List<Album> albums;
        private List<Shop> shops;
        private List<Stock> stocks;

        /// <summary>
        /// Gets or sets the IFormatProvider instance.
        /// </summary>
        public IFormatProvider Ifp { get; set; }

        /// <summary>
        /// Setup method to initialize (Mock, Repos, Logic, Instances).
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedAlbumRepo = new Mock<IAlbumRepository>(MockBehavior.Loose);
            this.mockedShopRepo = new Mock<IShopRepository>(MockBehavior.Loose);
            this.mockedStockRepo = new Mock<IStockRepository>(MockBehavior.Loose);

            this.mLogic = new ManagementLogic(this.mockedAlbumRepo.Object, this.mockedShopRepo.Object, this.mockedStockRepo.Object);

            this.albums = new List<Album>()
            {
                new Album() { Id = 1, Genre = "mulatós", Performer = "Aranyszemek", ReleaseYear = 2020, Title = "Aranyszemek aranyláncon", IsReissue = false },
                new Album() { Id = 2, Genre = "art-alter", Performer = "Kula Band", ReleaseYear = 2010, Title = "Kula zene felnőtteknek", IsReissue = false },
                new Album() { Id = 3, Genre = "izéke", Performer = "Müller Cecília", ReleaseYear = 2020, Title = "Nyunóka", IsReissue = true },
                new Album() { Id = 4, Genre = "halk zene", Performer = "Növény", ReleaseYear = 2015, Title = "Zene növényeknek", IsReissue = false },
                new Album() { Id = 5, Genre = "mulatós", Performer = "Csordás Robika", ReleaseYear = 2008, Title = "Csordás Robika küldi a kis Golyóba szeretettel", IsReissue = false },
            };

            this.shops = new List<Shop>()
            {
                new Shop() { ShopId = 1 },
                new Shop() { ShopId = 2, City = "Tar", ManagerName = "Z X", OpeningDate = DateTime.Parse("1967.4.14", this.Ifp), ShopName = "Üzlet" },
                new Shop() { ShopId = 3, City = "Csécse", ManagerName = "X Z", OpeningDate = DateTime.Parse("1988.12.1", this.Ifp), ShopName = "Vásárlási hely" },
            };

            this.stocks = new List<Stock>()
            {
                new Stock() { AlbumId = 1, ShopId = 1, Quantity = 1 },
                new Stock() { AlbumId = 2, ShopId = 2, Quantity = 2 },
                new Stock() { AlbumId = 3, ShopId = 3, Quantity = 3 },
                new Stock() { AlbumId = 4, ShopId = 4, Quantity = 4 },
                new Stock() { AlbumId = 5, ShopId = 5, Quantity = 5 },
            };
        }

        /// <summary>
        /// Testing if we get all the albums in one list.
        /// </summary>
        [Test]
        public void GetAllAlbumsTest()
        {
            this.mockedAlbumRepo.Setup(x => x.GetAll()).Returns(this.albums.AsQueryable());

            var result = this.mLogic.GetAllAlbums();

            this.mockedAlbumRepo.Verify(x => x.GetAll(), Times.Once);
            this.mockedAlbumRepo.Verify(x => x.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Testing if we get the wished Shop using the GetOne method.
        /// </summary>
        /// <param name="id">Id of the Shop we wish to get.</param>
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void TestGetOneShop(int id)
        {
            this.mockedShopRepo.Setup(x => x.GetAll()).Returns(this.shops.AsQueryable());
            this.mockedShopRepo.Setup(x => x.GetOne(It.IsAny<int>())).Returns(this.shops[id]);
            ManagementLogic mlogic = new ManagementLogic(this.mockedAlbumRepo.Object, this.mockedShopRepo.Object, this.mockedStockRepo.Object);

            var result = mlogic.GetShopById(id);

            this.mockedShopRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Testing the GetOne method with wrong parameters.
        /// </summary>
        /// <param name="id">Non-existing ids to get Albums.</param>
        [TestCase(-1)]
        [TestCase(-3)]
        [TestCase(-27)]
        public void TestGetOneAlbum(int id)
        {
            IndexOutOfRangeException ex = new IndexOutOfRangeException();

            this.mockedAlbumRepo.Setup(y => y.GetAll()).Returns(this.albums.AsQueryable());
            this.mockedAlbumRepo.Setup(y => y.GetOne(id)).Throws(ex);

            Assert.That(() => this.mLogic.GetAlbumById(id), Throws.TypeOf<IndexOutOfRangeException>());
            this.mockedAlbumRepo.Verify(repo => repo.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Testing whether we can remove a Shop by id.
        /// </summary>
        /// <param name="shopid">Id of the Shop to remove.</param>
        [TestCase(1)]
        public void TestRemoveShopIsCorrect(int shopid)
        {
            Shop shop = new Shop() { ShopId = shopid };

            this.mockedShopRepo.Setup(y => y.GetAll()).Returns(this.shops.AsQueryable());
            this.mockedShopRepo.Setup(x => x.GetOne(It.IsAny<int>())).Returns(this.shops[shopid]);
            this.mockedShopRepo.Setup(y => y.Remove(It.IsAny<int>())).Returns(true);

            bool result = this.mLogic.RemoveShop(shopid);

            Assert.That(result, Is.EqualTo(true));

            this.mockedShopRepo.Verify(y => y.Remove(shopid), Times.Once);
        }
    }
}
