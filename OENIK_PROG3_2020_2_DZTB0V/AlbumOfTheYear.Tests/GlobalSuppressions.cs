﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2201:Do not raise reserved exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Logic.Tests.PurchaserTests.TestRemoveNonExistCustomerIsIncorrect(System.Int32,System.String)")]
[assembly: SuppressMessage("Usage", "CA2201:Do not raise reserved exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Logic.Tests.ManagementTests.TestGetOneAlbum(System.Int32)")]
