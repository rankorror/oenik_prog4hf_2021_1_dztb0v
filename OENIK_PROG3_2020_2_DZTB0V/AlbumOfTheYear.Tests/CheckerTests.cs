﻿// <copyright file="CheckerTests.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Data.Model;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Logic.Results;
    using AlbumOfTheYear.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests fot the class: CheckerLogic.
    /// </summary>
    [TestFixture]
    public class CheckerTests
    {
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<IStockRepository> mockedStockRepo;
        private Mock<ICustomerRepository> mockedCustomerRepo;
        private Mock<IOrderRepository> mockedOrderRepo;
        private Mock<IShopRepository> mockedShopRepo;

        private CheckerLogic cLogic;

        private List<Album> albums;
        private List<Stock> stocks;
        private List<Customer> customers;
        private List<Order> orders;
        private List<Shop> shops;

        /// <summary>
        /// Gets or sets the Iformatprovider instance.
        /// </summary>
        public IFormatProvider Ifp { get; set; }

        /// <summary>
        /// Setup method to initialize.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedAlbumRepo = new Mock<IAlbumRepository>(MockBehavior.Loose);
            this.mockedCustomerRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            this.mockedOrderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            this.mockedStockRepo = new Mock<IStockRepository>(MockBehavior.Loose);
            this.mockedShopRepo = new Mock<IShopRepository>(MockBehavior.Loose);

            this.cLogic = new CheckerLogic(this.mockedStockRepo.Object, this.mockedShopRepo.Object, this.mockedCustomerRepo.Object, this.mockedOrderRepo.Object, this.mockedAlbumRepo.Object);

            this.albums = new List<Album>()
            {
                new Album() { Id = 1, Title = "Aranyszemek aranyláncon" },
                new Album() { Id = 2, Title = "Kula zene felnőtteknek" },
                new Album() { Id = 3, Title = "Nyunóka" },
                new Album() { Id = 4, Title = "Zene növényeknek" },
                new Album() { Id = 5, Title = "Csordás Robika küldi a kis Golyóba nagy szeretettel" },
            };

            this.customers = new List<Customer>()
            {
                new Customer() { CustomerId = 1, Name = "Valaki Vagyok" },
                new Customer() { CustomerId = 2, Address = "1094, Valahol, Utca 4.", Birthdate = DateTime.Parse("1976.11.26", this.Ifp), Email = "akarki@valami.hu", Name = "Akárki Vagyok", PhoneNumber = "+367098765432" },
                new Customer() { CustomerId = 3, Address = "2356, SeHol, Utca 65.", Birthdate = DateTime.Parse("1993.5.1", this.Ifp), Email = "senki@valami.hu", Name = "Senki Vagyok", PhoneNumber = "+367078965432" },

                // new Customer() { CustomerId = 4, Address = "8615, NéHol, Utca 1.", Birthdate = DateTime.Parse("1989.7.13", this.Ifp), Email = "barki@valami.hu", Name = "Bárki Vagyok", PhoneNumber = "+367086748194"},
            };

            this.orders = new List<Order>()
            {
                new Order() { OrderId = 1, AlbumId = 1, CustomerId = 1, ShopId = 1 },
                new Order() { OrderId = 2, AlbumId = 2, CustomerId = 1, OrderDate = DateTime.Parse("2020.12.5", this.Ifp), ShopId = 2 },
                new Order() { OrderId = 3, AlbumId = 3, CustomerId = 3, OrderDate = DateTime.Parse("2020.2.17", this.Ifp), ShopId = 3 },
            };

            this.stocks = new List<Stock>()
            {
                new Stock() { AlbumId = 1, ShopId = 1, Quantity = 70 },
                new Stock() { AlbumId = 2, ShopId = 1, Quantity = 30 },
                new Stock() { AlbumId = 1, ShopId = 3, Quantity = 3 },
                new Stock() { AlbumId = 4, ShopId = 3, Quantity = 4 },
                new Stock() { AlbumId = 5, ShopId = 5, Quantity = 5 },
                new Stock() { AlbumId = 3, ShopId = 1, Quantity = 1 },
            };

            this.shops = new List<Shop>()
            {
                new Shop() { ShopId = 1, City = "Tar", ManagerName = "Z X", OpeningDate = DateTime.Parse("1967.4.14", this.Ifp), ShopName = "Wave" },
                new Shop() { ShopId = 2, City = "Tar", ManagerName = "Z X", OpeningDate = DateTime.Parse("1967.4.14", this.Ifp), ShopName = "Üzlet" },
                new Shop() { ShopId = 3, City = "Csécse", ManagerName = "X Z", OpeningDate = DateTime.Parse("1988.12.1", this.Ifp), ShopName = "Vásárlási hely" },
            };
        }

        /// <summary>
        /// Testing if a given shops total stocknumber is calculated correctly.
        /// </summary>
        /// <param name="shopnumber">The number of shops in the mock.</param>
        [TestCase(3)]
        public void TestStockSums(int shopnumber)
        {
            List<StockSumResults> expected = new List<StockSumResults>()
            {
                new StockSumResults() { ShopId = 1, ShopName = "Wave", Quantity = 101 },
                new StockSumResults() { ShopId = 3, ShopName = "Vásárlási hely", Quantity = 7 },
            };

            this.mockedShopRepo.Setup(repo => repo.GetAll()).Returns(this.shops.AsQueryable());
            this.mockedStockRepo.Setup(repo => repo.GetAll()).Returns(this.stocks.AsQueryable());

            var result = this.cLogic.GetStockSums();

            Assert.That(result, Is.EquivalentTo(expected));

            this.mockedStockRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedShopRepo.Verify(repo => repo.GetAll(), Times.Exactly(shopnumber));
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Testing if the total number of orders by a given customer is being calculated correctly or not.
        /// </summary>
        /// <param name="numberOfOrders">Total number of orders in the mock.</param>
        [TestCase(3)]
        public void TestCustomerSums(int numberOfOrders)
        {
            List<CustomerSumResults> expected = new List<CustomerSumResults>()
            {
                new CustomerSumResults() { CustomerId = 1, CustomerName = "Valaki Vagyok", NumberOfOrders = 2 },
                new CustomerSumResults() { CustomerId = 3, CustomerName = "Senki Vagyok", NumberOfOrders = 1 },
            };

            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.customers.AsQueryable());
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable());

            var result = this.cLogic.GetCustomerSums();

            Assert.That(result, Is.EquivalentTo(expected));
            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedCustomerRepo.Verify(repo => repo.GetAll(), Times.Exactly(numberOfOrders));
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Testing whether the number of the shops where a given album is being sold and total number of pieces of albums on the market (of a given album) are being calculated correctly.
        /// </summary>
        /// <param name="numberOfStocks">The total number of Stocks in the mock.</param>
        [TestCase(6)]
        public void TestAlbumInShopsResults(int numberOfStocks)
        {
            List<AlbumInShopsResults> expected = new List<AlbumInShopsResults>()
            {
                new AlbumInShopsResults() { AlbumId = 1, AlbumName = "Aranyszemek aranyláncon", NumberOfAppearances = 2, NumberOfPieces = 73 },
                new AlbumInShopsResults() { AlbumId = 2, AlbumName = "Kula zene felnőtteknek",  NumberOfAppearances = 1, NumberOfPieces = 30 },
                new AlbumInShopsResults() { AlbumId = 3, AlbumName = "Nyunóka", NumberOfAppearances = 1, NumberOfPieces = 1 },
                new AlbumInShopsResults() { AlbumId = 4, AlbumName = "Zene növényeknek", NumberOfAppearances = 1, NumberOfPieces = 4 },
                new AlbumInShopsResults() { AlbumId = 5, AlbumName = "Csordás Robika küldi a kis Golyóba nagy szeretettel", NumberOfAppearances = 1, NumberOfPieces = 5 },
            };

            this.mockedAlbumRepo.Setup(repo => repo.GetAll()).Returns(this.albums.AsQueryable());
            this.mockedStockRepo.Setup(repo => repo.GetAll()).Returns(this.stocks.AsQueryable());

            var result = this.cLogic.GetAlbumInShopSums();

            Assert.That(result, Is.EquivalentTo(expected));
            this.mockedStockRepo.Verify(repo => repo.GetAll(), Times.Exactly(1));
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Exactly(numberOfStocks));
            this.mockedCustomerRepo.Verify(repo => repo.GetAll(), Times.Never);
        }
    }
}
