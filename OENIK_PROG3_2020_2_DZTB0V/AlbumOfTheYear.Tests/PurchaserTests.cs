﻿// <copyright file="PurchaserTests.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Data.Model;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the class: PurchaserLogic.
    /// </summary>
    [TestFixture]
    public class PurchaserTests
    {
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<IStockRepository> mockedStockRepo;
        private Mock<ICustomerRepository> mockedCustomerRepo;
        private Mock<IOrderRepository> mockedOrderRepo;

        private PurchaserLogic pLogic;

        private List<Album> albums;
        private List<Stock> stocks;
        private List<Customer> customers;
        private List<Order> orders;

        /// <summary>
        /// Gets or sets the IFormatProvider instance.
        /// </summary>
        public IFormatProvider Ifp { get; set; }

        /// <summary>
        /// Setup method to initialize (Mock, Repos, Logic, Instances).
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedAlbumRepo = new Mock<IAlbumRepository>(MockBehavior.Loose);
            this.mockedCustomerRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            this.mockedOrderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            this.mockedStockRepo = new Mock<IStockRepository>(MockBehavior.Loose);

            this.pLogic = new PurchaserLogic(this.mockedCustomerRepo.Object, this.mockedOrderRepo.Object, this.mockedStockRepo.Object, this.mockedAlbumRepo.Object);

            this.albums = new List<Album>()
            {
                new Album() { Id = 1, Genre = "mulatós", Performer = "Aranyszemek", ReleaseYear = 2020, Title = "Aranyszemek aranyláncon", IsReissue = false },
                new Album() { Id = 2, Genre = "art-alter", Performer = "Kula Band", ReleaseYear = 2010, Title = "Kula zene felnőtteknek", IsReissue = false },
                new Album() { Id = 3, Genre = "izéke", Performer = "Müller Cecília", ReleaseYear = 2020, Title = "Nyunóka", IsReissue = true },
                new Album() { Id = 4, Genre = "halk zene", Performer = "Növény", ReleaseYear = 2015, Title = "Zene növényeknek", IsReissue = false },
                new Album() { Id = 5, Genre = "mulatós", Performer = "Csordás Robika", ReleaseYear = 2008, Title = "Csordás Robika küldi a kis Golyóba szeretettel", IsReissue = false },
            };

            this.customers = new List<Customer>()
            {
                new Customer() { CustomerId = 1, Name = "Valaki Vagyok" },
                new Customer() { CustomerId = 2, Address = "1094, Valahol, Utca 4.", Birthdate = DateTime.Parse("1976.11.26", this.Ifp), Email = "akarki@valami.hu", Name = "Akárki Vagyok", PhoneNumber = "+367098765432" },
                new Customer() { CustomerId = 3, Address = "2356, SeHol, Utca 65.", Birthdate = DateTime.Parse("1993.5.1", this.Ifp), Email = "senki@valami.hu", Name = "Senki Vagyok", PhoneNumber = "+367078965432" },
                new Customer() { CustomerId = 4, Address = "8615, NéHol, Utca 1.", Birthdate = DateTime.Parse("1989.7.13", this.Ifp), Email = "barki@valami.hu", Name = "Bárki Vagyok", PhoneNumber = "+367086748194" },
            };

            this.orders = new List<Order>()
            {
                new Order() { OrderId = 1, AlbumId = 1, CustomerId = 1, ShopId = 1 },
                new Order() { OrderId = 2, AlbumId = 2, CustomerId = 2, OrderDate = DateTime.Parse("2020.12.5", this.Ifp), ShopId = 2 },
                new Order() { OrderId = 3, AlbumId = 3, CustomerId = 3, OrderDate = DateTime.Parse("2020.2.17", this.Ifp), ShopId = 3 },
            };

            this.stocks = new List<Stock>()
            {
                new Stock() { AlbumId = 1, ShopId = 1, Quantity = 1 },
                new Stock() { AlbumId = 2, ShopId = 2, Quantity = 2 },
                new Stock() { AlbumId = 3, ShopId = 3, Quantity = 3 },
                new Stock() { AlbumId = 4, ShopId = 4, Quantity = 4 },
                new Stock() { AlbumId = 5, ShopId = 5, Quantity = 5 },
            };
        }

        /// <summary>
        /// Testing whether inserting a new Order works well.
        /// </summary>
        [Test]
        public void TestInsertOrder()
        {
            int ordernum = this.orders.Count;

            this.mockedOrderRepo.Setup(y => y.GetAll()).Returns(this.orders.AsQueryable());
            this.mockedOrderRepo.Setup(y => y.Insert(It.IsAny<Order>())).Callback<Order>((Order order) => this.orders.Add(order));

            this.pLogic.AddOrder(3, 3, 4);

            var result = this.pLogic.GetAllOrders();

            this.mockedOrderRepo.Verify(y => y.Insert(It.IsAny<Order>()), Times.Once);
        }

        /// <summary>
        /// Testing whether updating a Customer (with full data) works well.
        /// </summary>
        /// <param name="id">Id of the Customer.</param>
        /// <param name="newaddress">New address of the Customer.</param>
        /// <param name="birthdate">Customer's new birthdate.</param>
        /// <param name="email">New email of the customer.</param>
        /// <param name="name">Name of the Customer.</param>
        /// <param name="newphone">New phonenumber of the Customer.</param>
        [TestCase(1, "1094, Valahol, Utca 4.", "1976.11.26", "akarki@valami.hu", "Akárki Vagyok", "+367098765432")]
        public void TestUpdateCustomer(int id, string newaddress, string birthdate, string email, string name, string newphone)
        {
            string[] newArray = new string[] { newaddress, birthdate, email, name, newphone };

            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.customers.AsQueryable());
            this.mockedCustomerRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(this.customers[id]);
            this.mockedCustomerRepo.Setup(repo => repo.CustomerUpdate(It.IsAny<int>(), It.IsAny<string[]>())).Callback((int id, string[] newArray) =>
            {
                this.customers[id].Address = newArray[0];
                this.customers[id].Birthdate = DateTime.Parse(newArray[1], this.Ifp);
                this.customers[id].Email = newArray[2];
                this.customers[id].Name = newArray[3];
                this.customers[id].PhoneNumber = newArray[4];
            });
            this.pLogic.UpdateCustomer(id, newArray);

            this.mockedCustomerRepo.Verify(repo => repo.CustomerUpdate(It.IsAny<int>(), It.IsAny<string[]>()), Times.Once);
        }

        /// <summary>
        /// Testing whether removing a customer with wrong id throws an exception or not.
        /// </summary>
        /// <param name="id">Non-existing id.</param>
        /// <param name="name">Name of the Customer.</param>
        [TestCase(8, "Valaki Vagyok")]
        public void TestRemoveNonExistCustomerIsIncorrect(int id, string name)
        {
            Customer c = new Customer() { CustomerId = id, Name = name };
            IndexOutOfRangeException ex = new IndexOutOfRangeException();

            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.customers.AsQueryable());
            this.mockedCustomerRepo.Setup(y => y.GetOne(id)).Throws(ex);

            Assert.That(() => this.pLogic.GetCustomerById(id), Throws.TypeOf<IndexOutOfRangeException>());
            this.mockedCustomerRepo.Verify(repo => repo.GetOne(id), Times.Once);
            this.mockedCustomerRepo.Verify(x => x.Remove(id), Times.Never);
        }
    }
}
