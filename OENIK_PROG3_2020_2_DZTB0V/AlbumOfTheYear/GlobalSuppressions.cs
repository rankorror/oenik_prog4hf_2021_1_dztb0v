﻿// <copyright file="GlobalSuppressions.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.ListAll(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.UpdateInstance(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.RemoveInstance(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.ShowSums(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.StockListing")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Program.Menu.AddInstance(System.String)")]
