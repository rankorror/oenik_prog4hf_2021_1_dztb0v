﻿// <copyright file="Program.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Program
{
    using System;
    using System.Linq;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Repository;
    using ConsoleTools;

    /// <summary>
    /// Main program class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Magic happens.
        /// </summary>
        private static void Main()
        {
            Factory f = new Factory();
            Menu m = new Menu(f);

            m.MyMenuInstall();
            m.Cm.Show();

            Console.ReadLine();
        }
    }
}
