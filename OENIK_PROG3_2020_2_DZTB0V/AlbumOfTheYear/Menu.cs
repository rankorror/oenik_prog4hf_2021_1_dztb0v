﻿// <copyright file="Menu.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Program
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Repository;
    using Castle.Core.Internal;
    using ConsoleTools;

    /// <summary>
    /// Menu class to create the menu separately.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="factory">Factory instance.</param>
        public Menu(Factory factory)
        {
            this.F = factory;
            this.ManagementL = this.F.MNGLGC;
            this.PurchaserL = this.F.PCSLGC;
            this.CheckerL = this.F.CHCKR;
        }

        /// <summary>
        /// Gets or sets my factory.
        /// </summary>
        public Factory F { get; set; }

        /// <summary>
        /// Gets or sets an Albumlogic instance.
        /// </summary>
        public ManagementLogic ManagementL { get; set; }

        /// <summary>
        /// Gets or sets a Shoplogic instance.
        /// </summary>
        public PurchaserLogic PurchaserL { get; set; }

        /// <summary>
        /// Gets or sets the Checker instance.
        /// </summary>
        public CheckerLogic CheckerL { get; set; }

        /// <summary>
        /// Gets or sets our ConsoleMenu intance.
        /// </summary>
        public ConsoleMenu Cm { get; set; }

        /// <summary>
        /// Gets or sets the IFP.
        /// </summary>
        public IFormatProvider IFP { get; set; }

        /// <summary>
        /// Heating up the Menu with loaded lists.
        /// </summary>
        public void MyMenuInstall()
        {
            this.Cm = new ConsoleMenu()
                .Add(" >> LIST ALL ALBUMS ", () => this.ListAll("Albums"))
                .Add(" >> LIST ALL SHOPS ", () => this.ListAll("Shops"))
                .Add(" >> LIST ALL ORDERS ", () => this.ListAll("Orders"))
                .Add(" >> LIST ALL STOCKS ", () => this.ListAll("Stocks"))
                .Add(" >> LIST ALL CUSTOMERS ", () => this.ListAll("Customers"))
                .Add(" >> ADD NEW ALBUM ", () => this.AddInstance("Album"))
                .Add(" >> ADD NEW SHOP ", () => this.AddInstance("Shop"))
                .Add(" >> ADD NEW ORDER ", () => this.AddInstance("Order"))
                .Add(" >> ADD NEW STOCK ", () => this.AddInstance("Stock"))
                .Add(" >> ADD NEW CUSTOMER ", () => this.AddInstance("Customer"))
                .Add(" >> REMOVE ALBUM BY IT'S ID ", () => this.RemoveInstance("Album"))
                .Add(" >> REMOVE SHOP BY IT'S ID ", () => this.RemoveInstance("Shop"))
                .Add(" >> REMOVE ORDER BY IT'S ID ", () => this.RemoveInstance("Order"))
                .Add(" >> REMOVE STOCK BY IT'S ID ", () => this.RemoveInstance("Stock"))
                .Add(" >> REMOVE CUSTOMER BY IT'S ID ", () => this.RemoveInstance("Customer"))
                .Add(" >> UPDATE ALBUM ", () => this.UpdateInstance("Album"))
                .Add(" >> UPDATE SHOP ", () => this.UpdateInstance("Shop"))
                .Add(" >> UPDATE ORDER ", () => this.UpdateInstance("Order"))
                .Add(" >> UPDATE STOCK ", () => this.UpdateInstance("Stock"))
                .Add(" >> UPDATE CUSTOMER ", () => this.UpdateInstance("Customer"))
                .Add(" >> CHECK THE QUANTITY OF THE SHOPS STOCKS: ", () => this.ShowSums("Stock"))
                .Add(" >> CHECK THE NUMBER OF ORDERS PER CUSTOMER: ", () => this.ShowSums("Customer"))
                .Add(" >> CHECK SOME NUMBERS CONCERNING THE ALBUMS: ", () => this.ShowSums("Album"))
                .Add(" >> LIST A STOCK OF A SHOP BY ID: ", () => this.StockListing())
                .Add(" >> CHECK THE QUANTITY OF THE SHOPS STOCKS (ASYNC): ", () => this.ShowSums("StockASYNC"))
                .Add(" >> CHECK THE NUMBER OF ORDERS PER CUSTOMER: (ASYNC)", () => this.ShowSums("CustomerASYNC"))
                .Add(" >> CHECK SOME NUMBERS CONCERNING THE ALBUMS: (ASYNC)", () => this.ShowSums("AlbumASYNC"))
                .Add(" >> EXIT ", () => this.Cm.CloseMenu());
        }

        /// <summary>
        /// Listing method.
        /// </summary>
        /// <param name="name">Name of the class we list of.</param>
        public void ListAll(string name)
        {
            Console.WriteLine("\n:: ALL INSTANCES ::\n");

            if (name == "Albums")
            {
                try
                {
                    this.ManagementL.GetAllAlbums().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                }
                catch (Exception)
                {
                    throw new ArgumentNullException("Albums", "ZERO ALBUM ON THIS PLANET!");
                }
            }
            else if (name == "Shops")
            {
                try
                {
                    this.ManagementL.GetAllShops().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                }
                catch (Exception)
                {
                    throw new ArgumentNullException("Shops", "ZERO SHOP ON THIS PLANET.");
                }
            }
            else if (name == "Stocks")
            {
                try
                {
                    this.ManagementL.GetAllStocks().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                }
                catch (Exception)
                {
                    throw new ArgumentNullException("Stocks", "ZERO STOCK ON THIS PLANET.");
                }
            }
            else if (name == "Customers")
            {
                try
                {
                    this.PurchaserL.GetAllCustomers().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                }
                catch (Exception)
                {
                    throw new ArgumentNullException("Customers", "ZERO CUSTOMER ON THIS PLANET.");
                }
            }
            else if (name == "Orders")
            {
                try
                {
                    this.PurchaserL.GetAllOrders().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                }
                catch (Exception)
                {
                    throw new ArgumentNullException("Orders", "NOBODY ORDERS IN DAYS LIKE THESE......");
                }
            }

            Console.WriteLine();
            Console.ReadLine();
        }

        /// <summary>
        /// Menu method for adding a new instance.
        /// </summary>
        /// <param name="name">Name of the class we add of.</param>
        public void AddInstance(string name)
        {
            if (name == "Album")
            {
                string newTitle;
                string newGenre;
                string newPerformer;
                int newRY;
                bool validator = true;

                do
                {
                    Console.Clear();
                    if (validator != true)
                    {
                        Console.WriteLine("TRY WITH SOME VALID DATA...");
                        Console.WriteLine();
                    }

                    Console.WriteLine("ENTER NEW TITLE:");
                    newTitle = Console.ReadLine();
                    Console.WriteLine("ENTER NEW GENRE:");
                    newGenre = Console.ReadLine();
                    Console.WriteLine("ENTER NEW PERFORMER:");
                    newPerformer = Console.ReadLine();
                    Console.WriteLine("ENTER NEW RELEASE YEAR:");
                    newRY = int.Parse(Console.ReadLine(), this.IFP);
                    validator = false;
                }
                while (newTitle.Length > 200 || newGenre.Length > 50 || newPerformer.Length > 150 || newRY > 2025 || newRY < 0);

                Console.WriteLine("IS IT A REISSUE EDITION? PRESS Y IF YES, N IF NO");
                string iR = Console.ReadLine();

                if (iR == "y" || iR == "Y")
                {
                    this.ManagementL.AddAlbum(newTitle, newGenre, newPerformer, newRY, true);
                }
                else
                {
                    this.ManagementL.AddAlbum(newTitle, newGenre, newPerformer, newRY, false);
                }
            }
            else if (name == "Shop")
            {
                string newName;
                string newBoss;
                string newCity;
                int newDY;
                int newDM;
                int newDD;
                bool validator = true;

                do
                {
                    Console.Clear();
                    if (validator != true)
                    {
                        Console.WriteLine("TRY WITH SOME VALID DATA...");
                        Console.WriteLine();
                    }

                    Console.WriteLine("ENTER NEW NAME:");
                    newName = Console.ReadLine();
                    Console.WriteLine("ENTER NEW NAME OF MANAGER:");
                    newBoss = Console.ReadLine();
                    Console.WriteLine("ENTER THE CITY:");
                    newCity = Console.ReadLine();
                    Console.WriteLine("ENTER NEW BIRTHYEAR:");
                    newDY = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER NEW BIRTHMONTH:");
                    newDM = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER NEW DAY OF BIRTH:");
                    newDD = int.Parse(Console.ReadLine(), this.IFP);
                    validator = false;
                }
                while (newName.Length > 50 || newBoss.Length > 100 || newCity.Length > 100 || newDY < 1850 || newDM < 1 || newDM > 12 || newDD < 1 || newDD > 31);

                this.ManagementL.AddShop(newName, newBoss, newCity, newDY, newDM, newDD);
            }
            else if (name == "Order")
            {
                int aID;
                int sID;
                int cID;
                bool validator = true;

                do
                {
                    if (!validator)
                    {
                        Console.Clear();
                        Console.WriteLine("SOME PROBLEMS HAVE OCCURED DURING THE ORDERING PROCESS. PLEASE TRY TO CHECK THE FOLLOWINGS:");
                        Console.WriteLine();
                        Console.WriteLine("1. YOU HAVE TO BE REGISTERED TO BE ABLE TO ORDER.");
                        Console.WriteLine("2. YOU ONLY CAN ORDER FROM A SHOP WHICH OWNS THE GIVEN ALBUM IN IT'S STOCK.");
                        Console.WriteLine("3. YOU ONLY CAN ORDER ONE ALBUM AT A TIME. BE PATIENT. EVERYONE LOVES MUSIC, NOT ONLY YOU.");
                        Console.WriteLine("4. CONSIDER TO TAKE A PEEK ON THE PAGE 'LIST A STOCK OF A SHOP BY ID' TO SEE WHICH SHOP OWNS THE ALBUM YOU'D LIKE TO BUY.");
                        Console.WriteLine();
                    }

                    Console.WriteLine();
                    Console.WriteLine("ENTER THE ID OF THE ALBUM:");
                    aID = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER THE ID OF THE SHOP:");
                    sID = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER YOUR ID:");
                    cID = int.Parse(Console.ReadLine(), this.IFP);

                    validator = false;

                    if (this.ManagementL.GetAlbumById(aID) != null && this.ManagementL.GetShopById(sID) != null && this.PurchaserL.GetCustomerById(cID) != null)
                    {
                        validator = true;
                    }
                }
                while (!validator);

                this.PurchaserL.AddOrder(sID, aID, cID);
            }
            else if (name == "Customer")
            {
                string newName;
                string newPhoneNumber;
                string newEmail;
                string newAddress;
                int newDY;
                int newDM;
                int newDD;
                bool validator = true;

                do
                {
                    Console.Clear();
                    if (validator != true)
                    {
                        Console.WriteLine("TRY WITH SOME VALID DATA...");
                        Console.WriteLine();
                    }

                    Console.WriteLine("ENTER NEW NAME:");
                    newName = Console.ReadLine();
                    Console.WriteLine("ENTER NEW PHONE NUMBER:");
                    newPhoneNumber = Console.ReadLine();
                    Console.WriteLine("ENTER NEW EMAIL:");
                    newEmail = Console.ReadLine();
                    Console.WriteLine("ENTER NEW ADDRESS:");
                    newAddress = Console.ReadLine();
                    Console.WriteLine("ENTER NEW BIRTHYEAR:");
                    newDY = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER NEW BIRTHMONTH:");
                    newDM = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER NEW DAY OF BIRTH:");
                    newDD = int.Parse(Console.ReadLine(), this.IFP);
                    validator = false;
                }
                while (newDY > 2002 || newDY < 1900 || newDM < 1 || newDM > 12 || newDD < 1 || newDD > 31 || newName.Length > 100 || newPhoneNumber.Length > 14 || newAddress.Length > 100 || newEmail.Length > 100);

                this.PurchaserL.AddCustomer(newName, newDY, newDM, newDD, newEmail, newAddress, newPhoneNumber);
            }
            else if (name == "Stock")
            {
                int aID;
                int sID;
                int qt;
                bool validator = true;

                do
                {
                    Console.Clear();

                    if (!validator)
                    {
                        Console.WriteLine();
                        Console.WriteLine("STOCKS CAN ONLY BE ADDED IF THE ALBUM AND THE SHOP EXIST IN THE DATABASE.");
                        Console.WriteLine("YOU CAN ADD NEW ALBUM OR NEW SHOP IN THE MAIN MENU.");
                        Console.WriteLine("MAYBE THE STOCK IS ALREADY INSERTED THAT YOU'RE TRYING TO ADD. TRY 'UPLOAD STOCK' INSTEAD.");
                        Console.WriteLine("ONLY A POSITIVE NUMBER CAN BE ACCEPTED AS THE STOCK'S QUANTITY.");
                        Console.WriteLine();
                    }

                    Console.WriteLine("ENTER THE ID OF THE ALBUM:");
                    aID = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER THE ID OF THE SHOP:");
                    sID = int.Parse(Console.ReadLine(), this.IFP);
                    Console.WriteLine("ENTER THE QUANTITY:");
                    qt = int.Parse(Console.ReadLine(), this.IFP);

                    validator = false;

                    if (this.ManagementL.GetStockByIds(aID, sID) == null && qt > 0)
                    {
                        validator = true;
                    }
                }
                while (!validator);

                this.ManagementL.AddStock(aID, sID, qt);
            }
        }

        /// <summary>
        /// Menu method for removing an instance.
        /// </summary>
        /// <param name="name">Name of the class we remove of.</param>
        public void RemoveInstance(string name)
        {
            if (name == "Album")
            {
                int aID;

                Console.WriteLine("ALL THE ALBUMS: ");
                Console.WriteLine();

                this.ManagementL.GetAllAlbums().ToList()
                    .ForEach(x => Console.WriteLine(x.MainData()));
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("ENTER THE ID OF THE ALBUM TO REMOVE:");
                aID = int.Parse(Console.ReadLine(), this.IFP);

                if (this.ManagementL.RemoveAlbum(aID))
                {
                    // GOOD
                }
                else
                {
                    Console.WriteLine("TRY AGAIN. WE CAN'T REMOVE WHAT'S NOT THERE, ANAKIN.");
                    Console.ReadLine();
                }
            }
            else if (name == "Shop")
            {
                int shID;

                Console.WriteLine("ALL THE SHOPS: ");
                Console.WriteLine();

                this.ManagementL.GetAllShops().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("ENTER THE ID OF THE SHOP TO REMOVE:");
                shID = int.Parse(Console.ReadLine(), this.IFP);

                if (this.ManagementL.RemoveShop(shID))
                {
                    // GOOD
                }
                else
                {
                    Console.WriteLine("TRY AGAIN. WE CAN'T REMOVE WHAT'S NOT THERE, ANAKIN.");
                    Console.ReadLine();
                }
            }
            else if (name == "Customer")
            {
                int cID;

                Console.WriteLine("ALL THE CUSTOMERS: ");
                Console.WriteLine();

                this.PurchaserL.GetAllCustomers().ToList()
                    .ForEach(x => Console.WriteLine(x.MainData()));
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("ENTER THE ID OF THE CUSTOMER TO REMOVE:");
                cID = int.Parse(Console.ReadLine(), this.IFP);

                if (this.PurchaserL.RemoveCustomer(cID))
                {
                    // GOOD
                }
                else
                {
                    Console.WriteLine("TRY AGAIN. WE CAN'T REMOVE WHAT'S NOT THERE, ANAKIN.");
                    Console.ReadLine();
                }
            }
            else if (name == "Stock")
            {
                int aID;
                int sID;

                Console.WriteLine("ALL THE STOCKS: ");
                Console.WriteLine();

                this.ManagementL.GetAllStocks().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("ENTER THE ALBUMID OF THE STOCK TO REMOVE:");
                aID = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER THE SHOPID OF THE STOCK TO REMOVE:");
                sID = int.Parse(Console.ReadLine(), this.IFP);

                if (this.ManagementL.RemoveStock(aID, sID))
                {
                    // GOOD
                }
                else
                {
                    Console.WriteLine("TRY AGAIN. WE CAN'T REMOVE WHAT'S NOT THERE, ANAKIN.");
                    Console.ReadLine();
                }
            }
            else if (name == "Order")
            {
                int oID;

                Console.WriteLine("ALL THE ORDERS: ");
                Console.WriteLine();

                this.PurchaserL.GetAllOrders().ToList()
                .ForEach(x => Console.WriteLine(x.MainData()));
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("ENTER THE ID OF THE ORDER TO REMOVE:");
                oID = int.Parse(Console.ReadLine(), this.IFP);

                if (this.PurchaserL.RemoveOrder(oID))
                {
                    // GOOD
                }
                else
                {
                    Console.WriteLine("TRY AGAIN. WE CAN'T REMOVE WHAT'S NOT THERE, ANAKIN.");
                    Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// Menu method for updating an instance.
        /// </summary>
        /// <param name="name">Name of the class we update of.</param>
        public void UpdateInstance(string name)
        {
            if (name == "Album")
            {
                int id;
                string newTitle;
                string newGenre;
                string newPerformer;
                string newRY;
                string[] data = new string[5];

                Console.WriteLine("ENTER THE ID OF THE ALBUM YOU WOULD LIKE TO UPDATE:");
                id = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER NEW TITLE:");
                newTitle = Console.ReadLine();
                Console.WriteLine("ENTER NEW GENRE:");
                newGenre = Console.ReadLine();
                Console.WriteLine("ENTER NEW PERFORMER:");
                newPerformer = Console.ReadLine();
                Console.WriteLine("ENTER NEW RELEASE YEAR:");
                newRY = Console.ReadLine();
                Console.WriteLine("IS IT A REISSUE EDITION? PRESS Y IF YES, N IF NO");
                string iR = Console.ReadLine();

                bool success = false;

                if (iR == "y" || iR == "Y")
                {
                    data[0] = newTitle;
                    data[1] = newPerformer;
                    data[2] = newGenre;
                    data[3] = newRY;
                    data[4] = "y";
                    success = this.ManagementL.UpdateAlbum(id, data);
                }
                else
                {
                    data[0] = newTitle;
                    data[1] = newPerformer;
                    data[2] = newGenre;
                    data[3] = newRY;
                    data[4] = "n";
                    success = this.ManagementL.UpdateAlbum(id, data);
                }

                Console.WriteLine();
                Console.WriteLine("ALBUM UPDATED: " + success);
                Console.ReadLine();
            }
            else if (name == "Shop")
            {
                string newName;
                string newBoss;
                string newCity;
                string newDY;
                string newDM;
                string newDD;
                string[] data = new string[6];
                int id;
                bool validator = true;

                Console.WriteLine("ENTER THE ID OF THE SHOP YOU WOULD LIKE TO UPDATE");
                id = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER NEW NAME:");
                newName = Console.ReadLine();
                Console.WriteLine("ENTER NEW NAME OF MANAGER:");
                newBoss = Console.ReadLine();
                Console.WriteLine("ENTER THE CITY:");
                newCity = Console.ReadLine();
                Console.WriteLine("ENTER NEW BIRTHYEAR:");
                newDY = Console.ReadLine();
                Console.WriteLine("ENTER NEW BIRTHMONTH:");
                newDM = Console.ReadLine();
                Console.WriteLine("ENTER NEW DAY OF BIRTH:");
                newDD = Console.ReadLine();
                validator = false;

                data[0] = newName;
                data[1] = newBoss;
                data[2] = newCity;
                data[3] = newDY;
                data[4] = newDM;
                data[5] = newDD;

                validator = this.ManagementL.ShopUpdate(id, data);

                Console.WriteLine();
                Console.WriteLine("SHOP UPDATED: " + validator);
                Console.ReadLine();
            }
            else if (name == "Order")
            {
                int id;
                int cID;
                int[] data = new int[1];
                bool validator = true;

                Console.WriteLine();
                Console.WriteLine("ENTER THE ID OF THE ORDER THAT YOU WOULD LIKE TO UPDATE (YOU ONLY CAN CHANGE THE PURCHASER): ");
                id = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER YOUR ID:");
                cID = int.Parse(Console.ReadLine(), this.IFP);

                data[0] = cID;

                validator = this.PurchaserL.OrderUpdate(id, data);

                Console.WriteLine();
                Console.WriteLine("ORDER UPDATED: " + validator);
                Console.ReadLine();
            }
            else if (name == "Customer")
            {
                string newName;
                string newPhoneNumber;
                string newEmail;
                string newAddress;
                string newDY;
                string newDM;
                string newDD;
                string[] data = new string[7];
                int id;
                bool validator = true;

                Console.WriteLine("ENTER THE ID OF THE CUSTOMER YOU WOULD LIKE TO UPDATE: ");
                id = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER NEW NAME:");
                newName = Console.ReadLine();
                Console.WriteLine("ENTER NEW PHONE NUMBER:");
                newPhoneNumber = Console.ReadLine();
                Console.WriteLine("ENTER NEW EMAIL:");
                newEmail = Console.ReadLine();
                Console.WriteLine("ENTER NEW ADDRESS:");
                newAddress = Console.ReadLine();
                Console.WriteLine("ENTER NEW BIRTHYEAR:");
                newDY = Console.ReadLine();
                Console.WriteLine("ENTER NEW BIRTHMONTH:");
                newDM = Console.ReadLine();
                Console.WriteLine("ENTER NEW DAY OF BIRTH:");
                newDD = Console.ReadLine();
                validator = false;

                data[0] = newName;
                data[1] = newDY;
                data[2] = newDM;
                data[3] = newDD;
                data[4] = newEmail;
                data[5] = newAddress;
                data[6] = newPhoneNumber;

                validator = this.PurchaserL.UpdateCustomer(id, data);

                Console.WriteLine();
                Console.WriteLine("CUSTOMER UPDATED: " + validator);
                Console.ReadLine();
            }
            else if (name == "Stock")
            {
                int aID;
                int sID;
                int qt;
                bool validator = true;

                Console.WriteLine("ENTER THE ID OF THE ALBUM:");
                aID = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER THE ID OF THE SHOP:");
                sID = int.Parse(Console.ReadLine(), this.IFP);
                Console.WriteLine("ENTER THE QUANTITY:");
                qt = int.Parse(Console.ReadLine(), this.IFP);

                validator = this.ManagementL.StockUpdate(aID, sID, qt) && qt > 0;

                Console.WriteLine();
                Console.WriteLine("STOCK UPDATED: " + validator);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu method for listing the results of queries.
        /// </summary>
        /// <param name="name">Name of the main class of the query.</param>
        public void ShowSums(string name)
        {
            if (name == "Stock")
            {
                foreach (var item in this.CheckerL.GetStockSums())
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }

            if (name == "Customer")
            {
                foreach (var item in this.CheckerL.GetCustomerSums())
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }

            if (name == "Album")
            {
                foreach (var item in this.CheckerL.GetAlbumInShopSums())
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }

            if (name == "StockASYNC")
            {
                var res = this.CheckerL.GetStockSumsAsync().Result;
                var task2 = this.CheckerL.GetStockSumsAsync();

                Console.Clear();
                Thread.Sleep(500);
                Console.WriteLine("Calculating.");
                Thread.Sleep(1000);
                Console.Clear();
                Console.WriteLine("Calculating..");
                Thread.Sleep(1500);
                Console.Clear();
                Console.WriteLine("Calculating...");
                Console.Clear();

                task2.Wait();

                var result = task2.Result;

                foreach (var item in result)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }

            if (name == "CustomerASYNC")
            {
                var res = this.CheckerL.GetCustomerSumsAsync().Result;
                var task2 = this.CheckerL.GetCustomerSumsAsync();

                Console.Clear();
                Thread.Sleep(500);
                Console.WriteLine("Calculating.");
                Thread.Sleep(1000);
                Console.Clear();
                Console.WriteLine("Calculating..");
                Thread.Sleep(1500);
                Console.Clear();
                Console.WriteLine("Calculating...");
                Console.Clear();

                task2.Wait();

                var result = task2.Result;

                foreach (var item in result)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }

            if (name == "AlbumASYNC")
            {
                var res = this.CheckerL.GetAlbumInShopSumsAsync().Result;
                var task2 = this.CheckerL.GetCustomerSumsAsync();

                Console.Clear();
                Thread.Sleep(500);
                Console.WriteLine("Calculating.");
                Thread.Sleep(1000);
                Console.Clear();
                Console.WriteLine("Calculating..");
                Thread.Sleep(1500);
                Console.Clear();
                Console.WriteLine("Calculating...");
                Console.Clear();

                task2.Wait();

                var result = task2.Result;

                foreach (var item in result)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine();
                Console.WriteLine("PRESS ENTER");
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu method to list the albums in stock of a shop.
        /// </summary>
        public void StockListing()
        {
            Console.Clear();
            this.ListAll("Shops");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("GIVE THE ID OF THE SHOP TO SEE IT'S STOCK: ");
            int id = int.Parse(Console.ReadLine(), this.IFP);
            Console.WriteLine();

            foreach (var item in this.PurchaserL.GetStockList(id))
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("PRESS ENTER");
            Console.ReadLine();
        }
    }
}
