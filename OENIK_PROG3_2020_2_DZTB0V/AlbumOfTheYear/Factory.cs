﻿// <copyright file="Factory.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Program
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Data.Model;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Repository;
    using ConsoleTools;

    /// <summary>
    /// Factory class.
    /// </summary>
    public class Factory
    {
        private static DbContext ctx = new DbContext();
        private static AlbumRepository albumrepo = new AlbumRepository(ctx);
        private static OrderRepository orderrepo = new OrderRepository(ctx);
        private static StockRepository stockrepo = new StockRepository(ctx);
        private static CustomerRepository customerrepo = new CustomerRepository(ctx);
        private static ShopRepository shoprepo = new ShopRepository(ctx);

        private ManagementLogic manage = new ManagementLogic(albumrepo, shoprepo, stockrepo);
        private PurchaserLogic purchase = new PurchaserLogic(customerrepo, orderrepo, stockrepo, albumrepo);
        private CheckerLogic check = new CheckerLogic(stockrepo, shoprepo, customerrepo, orderrepo, albumrepo);

        /// <summary>
        /// Gets or sets the ManagementLogic instance.
        /// </summary>
        public ManagementLogic MNGLGC
        {
            get { return this.manage; }
            set { this.manage = value; }
        }

        /// <summary>
        /// Gets or sets the PurchaserLogic instance.
        /// </summary>
        public PurchaserLogic PCSLGC
        {
            get { return this.purchase; }
            set { this.purchase = value; }
        }

        /// <summary>
        /// Gets or sets the Checker instance.
        /// </summary>
        public CheckerLogic CHCKR
        {
            get { return this.check; }
            set { this.check = value; }
        }
    }
}
