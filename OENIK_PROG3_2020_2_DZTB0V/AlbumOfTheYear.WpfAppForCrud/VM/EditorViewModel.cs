﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.WpfAppForCrud.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// ViewModel to Editor Window.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private AlbumCopy album;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.album = new AlbumCopy();
            if (this.IsInDesignMode)
            {
                this.album.Performer = "Polihudi";
                this.album.Genre = "konkrétan bármi";
                this.album.IsReissue = true;
                this.album.ReleaseYear = 1998;
                this.album.Title = "cím";
            }
        }

        /// <summary>
        /// Gets or sets an album instance.
        /// </summary>
        public AlbumCopy Album { get => this.album; set => this.Set(ref this.album, value); }
    }
}
