﻿// <copyright file="MainViewModel.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using AlbumOfTheYear.WpfAppForCrud.BL;
    using AlbumOfTheYear.WpfAppForCrud.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Viewmodel for the main window.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IAlbumLogic logic;

        /// <summary>
        /// The selected album.
        /// </summary>
        private AlbumCopy albumSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Albumlogic instance.</param>
        public MainViewModel(IAlbumLogic logic)
        {
            this.logic = logic;

            this.Albums = new ObservableCollection<AlbumCopy>();
            this.Albums = (ObservableCollection<AlbumCopy>)logic.GetAllAlbums();

            if (this.IsInDesignMode)
            {
                AlbumCopy a1 = new AlbumCopy()
                {
                    Performer = "Polihudi",
                    Genre = "konkrétan bármi",
                    IsReissue = true,
                    ReleaseYear = 1998,
                    Title = "cím",
                };

                AlbumCopy a2 = new AlbumCopy()
                {
                    Performer = "fndjsaigbna",
                    Genre = "fgdsjaogfasd",
                    IsReissue = false,
                    ReleaseYear = 2000,
                    Title = "gkmfdsoignfds",
                };

                this.Albums.Add(a1);
                this.Albums.Add(a2);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddAlbum(this.Albums));
            this.ModCmd = new RelayCommand(() => this.logic.ModAlbum(this.AlbumSelected));
            this.DelCmd = new RelayCommand(() => this.logic.DelAlbum(this.Albums, this.AlbumSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IAlbumLogic>())
        {
        }

        /// <summary>
        /// Gets the list of albums.
        /// </summary>
        public ObservableCollection<AlbumCopy> Albums { get; private set; }

        /// <summary>
        /// Gets or sets the selected album.
        /// </summary>
        public AlbumCopy AlbumSelected { get => this.albumSelected; set => this.Set(ref this.albumSelected, value); }

        /// <summary>
        /// Gets adding command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets deleting command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modifying command.
        /// </summary>
        public ICommand ModCmd { get; private set; }
    }
}
