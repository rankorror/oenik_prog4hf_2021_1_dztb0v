﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1115:Parameter should follow comma", Justification = "REVIEWED")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "REVIEWED", Scope = "member", Target = "~M:AlbumOfTheYear.WpfAppForCrud.BL.AlbumLogic.ModAlbum(AlbumOfTheYear.WpfAppForCrud.Data.AlbumCopy)")]
[assembly: SuppressMessage("Performance", "CA1812: Avoid uninstantiated internal classes", Justification = "<NikGitStats>", Scope = "module")]