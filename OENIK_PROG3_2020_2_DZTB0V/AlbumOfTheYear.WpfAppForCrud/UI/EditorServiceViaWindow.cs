﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.WpfAppForCrud.BL;
    using AlbumOfTheYear.WpfAppForCrud.Data;

    /// <summary>
    /// EditorService via Window.
    /// </summary>
    internal class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditAlbum(AlbumCopy a)
        {
            EditorWindow win = new EditorWindow(a);
            return win.ShowDialog() ?? false;
        }
    }
}
