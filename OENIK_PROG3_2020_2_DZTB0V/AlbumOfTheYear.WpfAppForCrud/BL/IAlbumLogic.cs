﻿// <copyright file="IAlbumLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.WpfAppForCrud.Data;

    /// <summary>
    /// Logic interface for albums.
    /// </summary>
    internal interface IAlbumLogic
    {
        /// <summary>
        /// Adding an album.
        /// </summary>
        /// <param name="list">The list we add to.</param>
        void AddAlbum(IList<AlbumCopy> list);

        /// <summary>
        /// Modifying an album.
        /// </summary>
        /// <param name="albumToModify">The album we modify.</param>
        void ModAlbum(AlbumCopy albumToModify);

        /// <summary>
        /// Deleting an album.
        /// </summary>
        /// <param name="list">The list we delete from.</param>
        /// <param name="album">The album we delete.</param>
        void DelAlbum(IList<AlbumCopy> list, AlbumCopy album);

        /// <summary>
        /// Getting a list of all the albums from the Db.
        /// </summary>
        /// <returns>A list of all the albums in the Db.</returns>
        IList<AlbumCopy> GetAllAlbums();
    }
}
