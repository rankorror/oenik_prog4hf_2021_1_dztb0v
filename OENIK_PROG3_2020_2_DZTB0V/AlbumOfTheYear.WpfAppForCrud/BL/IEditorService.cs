﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.WpfAppForCrud.Data;

    /// <summary>
    /// EditorService interface.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// Method for editing an album.
        /// </summary>
        /// <param name="a">Album instance.</param>
        /// <returns>A bool whether the edit was succesful or not.</returns>
        bool EditAlbum(AlbumCopy a);
    }
}
