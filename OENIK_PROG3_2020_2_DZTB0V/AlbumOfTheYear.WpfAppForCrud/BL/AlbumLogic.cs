﻿// <copyright file="AlbumLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.WpfAppForCrud.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Logic for albums.
    /// </summary>
    internal class AlbumLogic : IAlbumLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory f;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editorservice instance.</param>
        /// <param name="messengerService">Messengerservice instance.</param>
        /// <param name="f">Factory intance.</param>
        public AlbumLogic(IEditorService editorService, IMessenger messengerService, Factory f)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.f = f;
        }

        /// <inheritdoc/>
        public void AddAlbum(IList<AlbumCopy> list)
        {
            AlbumCopy newAlbum = new AlbumCopy();
            if (this.editorService.EditAlbum(newAlbum))
            {
                list.Add(newAlbum);
                this.f.MNGLGC.AddAlbum(newAlbum.Title, newAlbum.Genre, newAlbum.Performer, newAlbum.ReleaseYear, newAlbum.IsReissue);
                newAlbum.AlbumId = this.f.MNGLGC.GetAllAlbums().Last().Id;
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DelAlbum(IList<AlbumCopy> list, AlbumCopy album)
        {
            if (album != null && list.Remove(album))
            {
                this.f.MNGLGC.RemoveAlbum(album.AlbumId);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void ModAlbum(AlbumCopy albumToModify)
        {
            if (albumToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
            }

            AlbumCopy clone = new AlbumCopy();
            clone.CopyFrom(albumToModify);
            if (this.editorService.EditAlbum(clone) == true)
            {
                albumToModify.CopyFrom(clone);
                _ = this.f.MNGLGC.UpdateAlbum(
                    clone.AlbumId,
                    data: new string[] { clone.Title, clone.Performer, clone.Genre, clone.ReleaseYear.ToString(), clone.IsReissue.ToString(), });
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCELLED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<AlbumCopy> GetAllAlbums()
        {
            IList<Album> originalAlbums = this.f.MNGLGC.GetAllAlbums();
            IList<AlbumCopy> newAlbums = new ObservableCollection<AlbumCopy>();

            foreach (var album in originalAlbums)
            {
                newAlbums.Add(new AlbumCopy()
                {
                    AlbumId = album.Id,
                    Performer = album.Performer,
                    Genre = album.Genre,
                    ReleaseYear = album.ReleaseYear,
                    IsReissue = album.IsReissue,
                    Title = album.Title,
                });
            }

            return newAlbums;
        }
    }
}
