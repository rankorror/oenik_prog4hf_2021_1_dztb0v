﻿// <copyright file="AlbumCopy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfAppForCrud.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Copy of the album class.
    /// </summary>
    public class AlbumCopy : ObservableObject
    {
        /// <summary>
        /// Id of the album.
        /// </summary>
        private int albumId;

        /// <summary>
        /// Album title.
        /// </summary>
        private string title;

        /// <summary>
        /// Genre of the album.
        /// </summary>
        private string genre;

        /// <summary>
        /// The performer on this disque.
        /// </summary>
        private string performer;

        /// <summary>
        /// The year the album has been released.
        /// </summary>
        private int releaseYear;

        /// <summary>
        /// Bool that tells us whether the album is a reissue edition or not.
        /// </summary>
        private bool isReissue;

        /// <summary>
        /// Gets or sets the id of the album.
        /// </summary>
        public int AlbumId
        {
            get => this.albumId; set { this.Set(ref this.albumId, value); }
        }

        /// <summary>
        /// Gets or sets the album title.
        /// </summary>
        public string Title
        {
            get => this.title; set { this.Set(ref this.title, value); }
        }

        /// <summary>
        /// Gets or sets the genre of the album.
        /// </summary>
        public string Genre
        {
            get => this.genre; set { this.Set(ref this.genre, value); }
        }

        /// <summary>
        /// Gets or sets the performer on this disque.
        /// </summary>
        public string Performer
        {
            get => this.performer; set { this.Set(ref this.performer, value); }
        }

        /// <summary>
        /// Gets or sets the year the album has been released.
        /// </summary>
        public int ReleaseYear
        {
            get => this.releaseYear; set { this.Set(ref this.releaseYear, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the album is a reissue edition or not.
        /// </summary>
        public bool IsReissue
        {
            get => this.isReissue; set { this.Set(ref this.isReissue, value); }
        }

        /// <summary>
        /// Copying method.
        /// </summary>
        /// <param name="other">The album we copy properties from.</param>
        public void CopyFrom(AlbumCopy other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
