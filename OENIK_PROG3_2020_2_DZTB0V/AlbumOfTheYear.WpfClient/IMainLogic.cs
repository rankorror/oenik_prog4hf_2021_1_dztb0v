﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Logic interface for albums.
    /// </summary>
    internal interface IMainLogic
    {
        /// <summary>
        /// Editing an album.
        /// </summary>
        /// <param name="album">The album to edit.</param>
        /// <param name="editorFunc">The function to edit.</param>
        void EditAlbum(AlbumVM album, Func<AlbumVM, bool> editorFunc);

        /// <summary>
        /// Getting the albums list from json.
        /// </summary>
        /// <returns>A list of albums as AlbumVm instances.</returns>
        List<AlbumVM> ApiGetAlbums();

        /// <summary>
        /// Deleting an album.
        /// </summary>
        /// <param name="album">The album to delete.</param>
        void ApiDelAlbum(AlbumVM album);

        /// <summary>
        /// Editing or adding an album.
        /// </summary>
        /// <param name="album">The album to edit/add.</param>
        /// <param name="isEditing">Whether we edit or we add.</param>
        /// <returns>A bool indicating whether the operation was successful or not.</returns>
        public bool ApiEditAlbum(AlbumVM album, bool isEditing);

        /// <summary>
        /// Sending a message whether the operation was successful or not.
        /// </summary>
        /// <param name="success">The result whether the operation was successful or not.</param>
        public void SendMessage(bool success);
    }
}
