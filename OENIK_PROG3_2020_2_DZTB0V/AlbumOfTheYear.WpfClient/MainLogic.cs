﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Logic of Wpf Client project.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "https://localhost:44339/AlbumsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Sending a message whether the operation was successful or not.
        /// </summary>
        /// <param name="success">The result whether the operation was successful or not.</param>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "AlbumResult");
        }

        /// <summary>
        /// Getting the albums list from json.
        /// </summary>
        /// <returns>A list of albums as AlbumVm instances.</returns>
        public List<AlbumVM> ApiGetAlbums()
        {
            string uri = this.url + "all";
            string json = this.client.GetStringAsync(uri).Result;
            var list = JsonSerializer.Deserialize<List<AlbumVM>>(json, this.jsonOptions);

            return list;
        }

        /// <summary>
        /// Deleting an album.
        /// </summary>
        /// <param name="album">The album to delete.</param>
        public void ApiDelAlbum(AlbumVM album)
        {
            bool success = false;
            if (album != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + album.Id.ToString(CultureInfo.CurrentCulture)).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Editing or adding an album.
        /// </summary>
        /// <param name="album">The album to edit/add.</param>
        /// <param name="isEditing">Whether we edit or we add.</param>
        /// <returns>A bool indicating whether the operation was successful or not.</returns>
        public bool ApiEditAlbum(AlbumVM album, bool isEditing)
        {
            if (album == null)
            {
                return false;
            }

            if (album.Genre == null || album.Performer == null || album.ReleaseYear == 0 || album.Title == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", album.Id.ToString(CultureInfo.CurrentCulture));
            }

            postData.Add("title", album.Title);
            postData.Add("performer", album.Performer);
            postData.Add("genre", album.Genre);
            postData.Add("isReissue", album.IsReissue.ToString());
            postData.Add("releaseYear", album.ReleaseYear.ToString(CultureInfo.CurrentCulture));

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Editing an album.
        /// </summary>
        /// <param name="album">The album to edit.</param>
        /// <param name="editorFunc">The function to edit.</param>
        public void EditAlbum(AlbumVM album, Func<AlbumVM, bool> editorFunc)
        {
            AlbumVM clone = new AlbumVM();

            if (album != null)
            {
                clone.CopyFrom(album);
            }

            bool? success = editorFunc?.Invoke(clone);

            if (success == true)
            {
                if (album != null)
                {
                    success = this.ApiEditAlbum(clone, true);
                }
                else
                {
                    success = this.ApiEditAlbum(clone, false);
                }
            }

            this.SendMessage(success == true);
        }
    }
}
