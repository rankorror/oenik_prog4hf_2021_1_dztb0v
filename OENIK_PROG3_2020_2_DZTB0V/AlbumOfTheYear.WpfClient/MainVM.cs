﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main View Model.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private AlbumVM selectedAlbum;
        private ObservableCollection<AlbumVM> allAlbums;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">Logic of the project.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllAlbums = new ObservableCollection<AlbumVM>(this.logic.ApiGetAlbums()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelAlbum(this.SelectedAlbum));
            this.AddCmd = new RelayCommand(() => this.logic.EditAlbum(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditAlbum(this.SelectedAlbum, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the selected album to edit or to delete.
        /// </summary>
        public AlbumVM SelectedAlbum
        {
            get { return this.selectedAlbum; }
            set { this.selectedAlbum = value; }
        }

        /// <summary>
        /// Gets or sets the albums in collection.
        /// </summary>
        public ObservableCollection<AlbumVM> AllAlbums
        {
            get { return this.allAlbums; }
            set { this.Set(ref this.allAlbums, value); }
        }

        /// <summary>
        /// Gets or sets function for editing.
        /// </summary>
        public Func<AlbumVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets adding command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets deleting command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modifying command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets loading command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
