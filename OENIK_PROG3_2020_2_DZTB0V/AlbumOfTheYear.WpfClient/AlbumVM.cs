﻿// <copyright file="AlbumVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Album in WPF Client project.
    /// </summary>
    public class AlbumVM : ObservableObject
    {
        /// <summary>
        /// Id of the album.
        /// </summary>
        private int id;

        /// <summary>
        /// Album title.
        /// </summary>
        private string title;

        /// <summary>
        /// Genre of the album.
        /// </summary>
        private string genre;

        /// <summary>
        /// The performer on this disque.
        /// </summary>
        private string performer;

        /// <summary>
        /// The year the album has been released.
        /// </summary>
        private int releaseYear;

        /// <summary>
        /// Bool that tells us whether the album is a reissue edition or not.
        /// </summary>
        private bool isReissue;

        /// <summary>
        /// Gets or sets the id of the album.
        /// </summary>
        public int Id
        {
            get => this.id; set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the album title.
        /// </summary>
        public string Title
        {
            get => this.title; set { this.Set(ref this.title, value); }
        }

        /// <summary>
        /// Gets or sets the genre of the album.
        /// </summary>
        public string Genre
        {
            get => this.genre; set { this.Set(ref this.genre, value); }
        }

        /// <summary>
        /// Gets or sets the performer on this disque.
        /// </summary>
        public string Performer
        {
            get => this.performer; set { this.Set(ref this.performer, value); }
        }

        /// <summary>
        /// Gets or sets the year the album has been released.
        /// </summary>
        public int ReleaseYear
        {
            get => this.releaseYear; set { this.Set(ref this.releaseYear, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the album is a reissue edition or not.
        /// </summary>
        public bool IsReissue
        {
            get => this.isReissue; set { this.Set(ref this.isReissue, value); }
        }

        /// <summary>
        /// Copying method.
        /// </summary>
        /// <param name="other">The album we copy properties from.</param>
        public void CopyFrom(AlbumVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Id = other.Id;
            this.Title = other.Title;
            this.Performer = other.Performer;
            this.Genre = other.Genre;
            this.ReleaseYear = other.ReleaseYear;
            this.IsReissue = other.IsReissue;
        }
    }
}
