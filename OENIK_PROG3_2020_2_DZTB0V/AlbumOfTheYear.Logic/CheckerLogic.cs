﻿// <copyright file="CheckerLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Logic.Interfaces;
    using AlbumOfTheYear.Logic.Results;
    using AlbumOfTheYear.Repository;

    /// <summary>
    /// Executing queries with the repos.
    /// </summary>
    public class CheckerLogic : ISumChecker
    {
        private readonly IStockRepository srp;

        private readonly IShopRepository shrp;

        private readonly ICustomerRepository crp;

        private readonly IOrderRepository or;

        private readonly IAlbumRepository ar;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckerLogic"/> class.
        /// </summary>
        /// <param name="sr">Stockrepo interface.</param>
        /// <param name="shrp">Shoprepo interface.</param>
        /// <param name="crp">Customerrepo interface.</param>
        /// <param name="or">Orderrepo interface.</param>
        /// <param name="ar">Albumrepo interface.</param>
        public CheckerLogic(IStockRepository sr, IShopRepository shrp, ICustomerRepository crp, IOrderRepository or, IAlbumRepository ar)
        {
            this.srp = sr;
            this.shrp = shrp;
            this.crp = crp;
            this.or = or;
            this.ar = ar;
        }

        /// <summary>
        /// Giving back a list of StockSumResults that shows the quantity of albums in each shop.
        /// </summary>
        /// <returns>A list of StockSumResults that shows the quantity of albums in each shop.</returns>
        public IList<StockSumResults> GetStockSums()
        {
            var q = from stock in this.srp.GetAll()
                    join sh in this.shrp.GetAll()
                    on stock.ShopId equals sh.ShopId
                    group stock by stock.ShopId into grp
                    select new StockSumResults()
                    {
                        ShopId = grp.Key,
                        ShopName = this.shrp.GetAll().Where(x => x.ShopId == grp.Key).FirstOrDefault().ShopName,
                        Quantity = grp.Sum(x => x.Quantity),
                    };

            return q.ToList();
        }

        /// <summary>
        /// Giving back a list of CustomerSumResults that shows the number of orders for each customer.
        /// </summary>
        /// <returns>A list of CustomerSumResults that shows the number of orders for each customer.</returns>
        public IList<CustomerSumResults> GetCustomerSums()
        {
            var q = from order in this.or.GetAll()
                    join customer in this.crp.GetAll()
                    on order.CustomerId equals customer.CustomerId
                    group order by order.CustomerId into grp
                    select new CustomerSumResults()
                    {
                        CustomerId = grp.Key,
                        CustomerName = this.crp.GetAll().Where(x => x.CustomerId == grp.Key).FirstOrDefault().Name,
                        NumberOfOrders = grp.Count(),
                    };

            return q.ToList();
        }

        /// <summary>
        /// Giving back a list of AlbumInShopsResults that shows the number of shops where the given album is currently in stock and the number of instances of the given album that are being sold.
        /// </summary>
        /// <returns>A list of AlbumInShopsResults that shows the number of shops where the given album is currently in stock and the number of instances of the given album that are being sold.</returns>
        public IList<AlbumInShopsResults> GetAlbumInShopSums()
        {
            var q = from stock in this.srp.GetAll()
                    join album in this.ar.GetAll()
                    on stock.AlbumId equals album.Id
                    group stock by stock.AlbumId into grp
                    select new AlbumInShopsResults()
                    {
                        AlbumId = grp.Key,
                        AlbumName = this.ar.GetAll().Where(x => x.Id == grp.Key).FirstOrDefault().Title,
                        NumberOfAppearances = grp.Count(),
                        NumberOfPieces = grp.Sum(x => x.Quantity),
                    };

            return q.ToList();
        }

        /// <summary>
        /// Async version of GetStockSum non-crud method.
        /// </summary>
        /// <returns>A task of lists of results.</returns>
        public Task<IList<StockSumResults>> GetStockSumsAsync()
        {
            return Task.Run(() => this.GetStockSums());
        }

        /// <summary>
        /// Async version of GetCustomerSum non-crud method.
        /// </summary>
        /// <returns>A task of lists of results.</returns>
        public Task<IList<CustomerSumResults>> GetCustomerSumsAsync()
        {
            return Task.Run(() => this.GetCustomerSums());
        }

        /// <summary>
        /// Async version of GetAlbumInShopSums non-crud method.
        /// </summary>
        /// <returns>A task of lists of results.</returns>
        public Task<IList<AlbumInShopsResults>> GetAlbumInShopSumsAsync()
        {
            return Task.Run(() => this.GetAlbumInShopSums());
        }
    }
}
