﻿// <copyright file="PurchaserLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Data.Model;
    using AlbumOfTheYear.Logic.Interfaces;
    using AlbumOfTheYear.Logic.Results;
    using AlbumOfTheYear.Repository;

    /// <summary>
    /// Logic for the customer/purchaser administration.
    /// </summary>
    public class PurchaserLogic : ICustomerLogic, IOrderLogic, IStockList
    {
        private readonly ICustomerRepository customerrepo;

        private readonly IOrderRepository orderrepo;

        private readonly IStockRepository stockrepo;

        private readonly IAlbumRepository albumrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaserLogic"/> class.
        /// </summary>
        /// <param name="cr">Customerrepo.</param>
        /// <param name="or">Orderrepo.</param>
        /// <param name="sr">Stockrepo.</param>
        /// <param name="ar">Albumrepo.</param>
        public PurchaserLogic(ICustomerRepository cr, IOrderRepository or, IStockRepository sr, IAlbumRepository ar)
        {
            this.customerrepo = cr;
            this.orderrepo = or;
            this.stockrepo = sr;
            this.albumrepo = ar;
        }

        /// <summary>
        /// Gets or sets the IFP.
        /// </summary>
        public IFormatProvider IFP { get; set; }

        // UPDATING THE DB...

        /// <inheritdoc/>
        public bool UpdateCustomer(int id, string[] data)
        {
            if (this.customerrepo.GetAll().ToList().Contains(this.GetCustomerById(id)))
            {
                    this.customerrepo.CustomerUpdate(id, data);
                    return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool OrderUpdate(int id, int[] data)
        {
            if (this.orderrepo.GetAll().ToList().Contains(this.GetOrderById(id)))
            {
                    this.orderrepo.OrderUpdate(id, data);
                    return true;
            }

            return false;
        }

        // LISTS OF THE TABLES

        /// <inheritdoc/>
        public IList<Customer> GetAllCustomers()
        {
            return this.customerrepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Order> GetAllOrders()
        {
            return this.orderrepo.GetAll().ToList();
        }

        // ADDING TO THE DB

        /// <inheritdoc/>
        public bool AddCustomer(string name, int birthYear, int birthMonth, int birthDay, string email, string address, string phoneNumber)
        {
            Customer newCustomer = new Customer
            {
                Name = name,
                Birthdate = DateTime.Parse(birthYear + "." + birthMonth + "." + birthDay, this.IFP),
                Email = email,
                PhoneNumber = phoneNumber,
                Address = address,
            };

            this.customerrepo.Insert(newCustomer);

            return true;
        }

        /// <inheritdoc/>
        public bool AddOrder(int shopId, int albumId, int customerId)
        {
            Order ord = new Order()
            {
                AlbumId = albumId,
                ShopId = shopId,
                CustomerId = customerId,
                OrderDate = DateTime.Now,
            };

            this.orderrepo.Insert(ord);

            return true;
        }

        // REMOVING FROM THE DB

        /// <inheritdoc/>
        public bool RemoveCustomer(int id)
        {
            return this.customerrepo.Remove(id);
        }

        /// <inheritdoc/>
        public bool RemoveOrder(int id)
        {
            return this.orderrepo.Remove(id);
        }

        /// <inheritdoc/>
        public IList<StockListResults> GetStockList(int id)
        {
            var q = from stock in this.stockrepo.GetAll().Where(x => x.ShopId == id)
                    join album in this.albumrepo.GetAll()
                    on stock.AlbumId equals album.Id
                    select new StockListResults()
                    {
                        AlbumId = album.Id,
                        AlbumName = album.Title,
                        Number = stock.Quantity,
                        ShopId = id,
                        Reissue = album.IsReissue,
                        Performer = album.Performer,
                    };

            return q.ToList();
        }

        /// <inheritdoc/>
        public Customer GetCustomerById(int id)
        {
            return this.customerrepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Order GetOrderById(int id)
        {
            return this.orderrepo.GetOne(id);
        }
    }
}
