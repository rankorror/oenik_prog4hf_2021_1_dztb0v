﻿// <copyright file="AlbumInShopsResults.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Results
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Result class for the query "AlbumInShops".
    /// </summary>
    public class AlbumInShopsResults
    {
        /// <summary>
        /// Gets or sets the name of the album.
        /// </summary>
        public string AlbumName { get; set; }

        /// <summary>
        /// Gets or sets the number of appearances of albums in shops.
        /// </summary>
        public int NumberOfAppearances { get; set; }

        /// <summary>
        /// Gets or sets the quantity of each album in a every shop.
        /// </summary>
        public int NumberOfPieces { get; set; }

        /// <summary>
        /// Gets or sets the album id.
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        /// Overriding the tostring method to have a formatted string to write on console.
        /// </summary>
        /// <returns>A formatted string to write on console.</returns>
        public override string ToString()
        {
            return $"NAME OF ALBUM = {this.AlbumName}, NUMBER OF SHOPS IT IS BEING SOLD: {this.NumberOfAppearances}, NUMBER OF AVAILABLE PIECES OUT THERE ON THE MARKET: {this.NumberOfPieces}";
        }

        /// <summary>
        /// Overriding to examine whether two instances of this class are equals or not.
        /// </summary>
        /// <param name="obj">The other object of this class.</param>
        /// <returns>A bool that tells whether the two instnces are equals or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AlbumInShopsResults)
            {
                AlbumInShopsResults other = obj as AlbumInShopsResults;
                return this.AlbumId == other.AlbumId && this.NumberOfAppearances == other.NumberOfAppearances && this.NumberOfPieces == other.NumberOfPieces;
            }

            return false;
        }

        /// <summary>
        /// Gets a hash code from the data of this class.
        /// </summary>
        /// <returns>A hash code.</returns>
        public override int GetHashCode()
        {
            return this.AlbumId.GetHashCode() + this.NumberOfPieces + this.NumberOfAppearances;
        }
    }
}
