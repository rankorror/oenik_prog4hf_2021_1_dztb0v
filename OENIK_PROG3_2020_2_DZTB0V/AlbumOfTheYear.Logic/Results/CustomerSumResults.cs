﻿// <copyright file="CustomerSumResults.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Results
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Result class for the query "CustomerSum".
    /// </summary>
    public class CustomerSumResults
    {
        /// <summary>
        /// Gets or sets the name of the Customer.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the number of orders of the Customer.
        /// </summary>
        public int NumberOfOrders { get; set; }

        /// <summary>
        /// Gets or sets the id of the Customer.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Overriding the tostring method to have a formatted string to write on console.
        /// </summary>
        /// <returns>A formatted string to write on console.</returns>
        public override string ToString()
        {
            return $"NAME OF THE CUSTOMER = {this.CustomerName}, NUMBER OF ORDERS RELATED: {this.NumberOfOrders}";
        }

        /// <summary>
        /// Overriding the equals method to examine whether two instances of this class are equals.
        /// </summary>
        /// <param name="obj">The other intance.</param>
        /// <returns>A bool that tells whether the two instances are equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is CustomerSumResults)
            {
                CustomerSumResults other = obj as CustomerSumResults;
                return this.CustomerId == other.CustomerId && this.NumberOfOrders == other.NumberOfOrders;
            }

            return false;
        }

        /// <summary>
        /// Gets a hash code from the data of this class.
        /// </summary>
        /// <returns>A hash code.</returns>
        public override int GetHashCode()
        {
            return this.CustomerId.GetHashCode() + this.NumberOfOrders;
        }
    }
}
