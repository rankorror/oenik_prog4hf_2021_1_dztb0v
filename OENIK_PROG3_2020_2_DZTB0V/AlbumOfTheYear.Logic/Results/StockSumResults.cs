﻿// <copyright file="StockSumResults.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Result class for the query "StockSum".
    /// </summary>
    public class StockSumResults
    {
        /// <summary>
        /// Gets or sets the name of the Shop.
        /// </summary>
        public string ShopName { get; set; }

        /// <summary>
        /// Gets or sets the id of the Shop.
        /// </summary>
        public int ShopId { get; set; }

        /// <summary>
        /// Gets or sets the quantity of this Shop.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Overriding the tostring method to have a formatted string to write to console.
        /// </summary>
        /// <returns>A formatted string to write to console.</returns>
        public override string ToString()
        {
            return $"NAME OF THE SHOP = {this.ShopName}, QUANTITY = {this.Quantity}";
        }

        /// <summary>
        /// Overriding the equals method to examine whether two instances of this class are equal.
        /// </summary>
        /// <param name="obj">The other instance.</param>
        /// <returns>A bool that tells whether the two instances are equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is StockSumResults)
            {
                StockSumResults other = obj as StockSumResults;
                return this.ShopId == other.ShopId && this.Quantity == other.Quantity;
            }

            return false;
        }

        /// <summary>
        /// Gets a hash code from the data of this class.
        /// </summary>
        /// <returns>A hash code.</returns>
        public override int GetHashCode()
        {
            return this.ShopId.GetHashCode() + this.Quantity;
        }
    }
}
