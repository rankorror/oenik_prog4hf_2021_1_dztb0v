﻿// <copyright file="StockListResults.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Results
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// Result class for the query "StockList".
    /// </summary>
    public class StockListResults
    {
        /// <summary>
        /// Gets or sets the name of the album.
        /// </summary>
        public string AlbumName { get; set; }

        /// <summary>
        /// Gets or sets the performer of the album.
        /// </summary>
        public string Performer { get; set; }

        /// <summary>
        /// Gets or sets the album id.
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        /// Gets or sets the number of present albums in the shop.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the id of the shop.
        /// </summary>
        public int ShopId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the album is a reissue edition or not.
        /// </summary>
        public bool Reissue { get; set; }

        /// <summary>
        /// Overriding the tostring method to have a formatted string to write to console.
        /// </summary>
        /// <returns>A formatted string to write to console.</returns>
        public override string ToString()
        {
            string re;

            if (this.Reissue == true)
            {
                re = "YES";
            }
            else
            {
                re = "NO";
            }

            return $"TITLE: {this.AlbumName}, PERFORMER: {this.Performer}, REISSUE: {re}, QUANTITY: {this.Number}";
        }

        /// <summary>
        /// Overriding to exmine whether two instances of this class are equals.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>A bool that tells os whether the to objects are the same or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is StockListResults)
            {
                StockListResults other = obj as StockListResults;
                return this.AlbumId == other.AlbumId && this.ShopId == other.ShopId && this.Number == other.Number;
            }

            return false;
        }

        /// <summary>
        /// Gets the hash code from the instance's data.
        /// </summary>
        /// <returns>The hash code from the instance's data.</returns>
        public override int GetHashCode()
        {
            return this.AlbumId.GetHashCode() + this.Number + this.ShopId.GetHashCode();
        }
    }
}
