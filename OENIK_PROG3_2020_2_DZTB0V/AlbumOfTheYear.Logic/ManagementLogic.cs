﻿// <copyright file="ManagementLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using AlbumOfTheYear.Repository;

    /// <summary>
    /// Logic for management administration.
    /// </summary>
    public class ManagementLogic : IAlbumLogic, IShopLogic, IStockLogic
    {
        private readonly IAlbumRepository albumrepo;

        private readonly IShopRepository shoprepo;

        private readonly IStockRepository stockrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagementLogic"/> class.
        /// </summary>
        /// <param name="ar">Albumrepo.</param>
        /// <param name="shr">Shoprepo.</param>
        /// <param name="sr">Stockrepo.</param>
        public ManagementLogic(IAlbumRepository ar, IShopRepository shr, IStockRepository sr)
        {
            this.albumrepo = ar;
            this.shoprepo = shr;
            this.stockrepo = sr;
        }

        /// <summary>
        /// Gets or sets the IFP.
        /// </summary>
        public IFormatProvider IFP { get; set; }

        // UPDATE...

        /// <inheritdoc/>
        public bool UpdateAlbum(int id, string[] data)
        {
            if (this.albumrepo.GetAll().ToList().Contains(this.GetAlbumById(id)))
            {
                    this.albumrepo.AlbumUpdate(id, data);
                    return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool StockUpdate(int id, int id2, int quantity)
        {
            if (this.stockrepo.GetAll().ToList().Contains(this.GetStockByIds(id, id2)))
            {
                try
                {
                    this.stockrepo.UpdateStock(id, id2, quantity);
                    return true;
                }
                catch (NullReferenceException e)
                {
                    throw new NullReferenceException(e.Message);
                }
                catch (ArgumentException e)
                {
                    throw new ArgumentException(e.Message);
                }
                catch (FormatException e)
                {
                    throw new FormatException(e.Message);
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public bool ShopUpdate(int id, string[] data)
        {
            if (this.shoprepo.GetAll().ToList().Contains(this.GetShopById(id)))
            {
                try
                {
                    this.shoprepo.UpdateShop(id, data);
                    return true;
                }
                catch (NullReferenceException e)
                {
                    throw new NullReferenceException(e.Message);
                }
                catch (ArgumentException e)
                {
                    throw new ArgumentException(e.Message);
                }
                catch (FormatException e)
                {
                    throw new FormatException(e.Message);
                }
            }

            return false;
        }

        // GET BY ID

        /// <inheritdoc/>
        public Album GetAlbumById(int id)
        {
            return this.albumrepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Shop GetShopById(int id)
        {
            return this.shoprepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Stock GetStockByIds(int id, int id2)
        {
            return this.stockrepo.GetOne(id, id2);
        }

        // LISTS OF THE TABLES

        /// <inheritdoc/>
        public IList<Album> GetAllAlbums()
        {
            return this.albumrepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Shop> GetAllShops()
        {
            return this.shoprepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Stock> GetAllStocks()
        {
            return this.stockrepo.GetAll().ToList();
        }

        // ADDING TO THE DB

        /// <inheritdoc/>
        public bool AddAlbum(string title, string genre, string performer, int releaseYear, bool isReissue)
        {
            Album a = new Album()
            {
                Title = title,
                Genre = genre,
                Performer = performer,
                ReleaseYear = releaseYear,
                IsReissue = isReissue,
            };

            this.albumrepo.Insert(a);

            return true;
        }

        /// <inheritdoc/>
        public bool AddShop(string name, string bossName, string city, int openingYear, int openingMonth, int openingDay)
        {
            Shop s = new Shop()
            {
                ShopName = name,
                ManagerName = bossName,
                City = city,
                OpeningDate = DateTime.Parse(openingYear + "." + openingMonth + "." + openingDay, this.IFP),
            };

            this.shoprepo.Insert(s);

            return true;
        }

        /// <inheritdoc/>
        public bool AddStock(int albumId, int shopId, int qt)
        {
            Stock st = new Stock()
            {
                AlbumId = albumId,
                ShopId = shopId,
                Quantity = qt,
            };

            this.stockrepo.Insert(st);
            return true;
        }

        // REMOVING FROM THE DB

        /// <inheritdoc/>
        public bool RemoveAlbum(int id)
        {
            return this.albumrepo.Remove(id);
        }

        /// <inheritdoc/>
        public bool RemoveShop(int id)
        {
            return this.shoprepo.Remove(id);
        }

        /// <inheritdoc/>
        public bool RemoveStock(int id, int id2)
        {
            return this.stockrepo.Remove(id, id2);
        }
    }
}
