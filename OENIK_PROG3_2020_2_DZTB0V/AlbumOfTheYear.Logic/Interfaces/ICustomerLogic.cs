﻿// <copyright file="ICustomerLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The specific Customer logic interface.
    /// </summary>
    public interface ICustomerLogic
    {
        /// <summary>
        /// Make a list of all the customers.
        /// </summary>
        /// <returns>A list of all the customers.</returns>
        IList<Customer> GetAllCustomers();

        /// <summary>
        /// Picking the customer by it's id.
        /// </summary>
        /// <param name="id">Id of the chosen customer.</param>
        /// <returns>A Customer instance.</returns>
        Customer GetCustomerById(int id);

        /// <summary>
        /// Adding a new Customer to the Db.
        /// </summary>
        /// <param name="name">Name of the Customer.</param>
        /// <param name="birthYear">Customer's birthyear.</param>
        /// <param name="birthMonth">Customer's birthmont.</param>
        /// <param name="birthDay">Customer's day of birth.</param>
        /// <param name="email">Customer's email.</param>
        /// <param name="address">Customer's address.</param>
        /// <param name="phoneNumber">Customer's phone address.</param>
        /// <returns>A bool whether the insert was successful or nor.</returns>
        public bool AddCustomer(string name, int birthYear, int birthMonth, int birthDay, string email, string address, string phoneNumber);

        /// <summary>
        /// Removing a Customer from the Db.
        /// </summary>
        /// <param name="id">Id of the Customer.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool RemoveCustomer(int id);

        /// <summary>
        /// Updating a Customer with data.
        /// </summary>
        /// <param name="id">Id of the Customer.</param>
        /// <param name="data">Data we upload.</param>
        /// <returns>Whether the update was successful or not.</returns>
        bool UpdateCustomer(int id, string[] data);
    }
}
