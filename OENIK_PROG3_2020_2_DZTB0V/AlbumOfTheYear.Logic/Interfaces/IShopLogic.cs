﻿// <copyright file="IShopLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The interface where we give the methods.
    /// </summary>
    public interface IShopLogic
    {
        /// <summary>
        /// Picking the Shop by it's id.
        /// </summary>
        /// <param name="id">Id of the chosen shop.</param>
        /// <returns>A Shop instance.</returns>
        Shop GetShopById(int id);

        /// <summary>
        /// Make a list of all the shops.
        /// </summary>
        /// <returns>A list of all the shops.</returns>
        IList<Shop> GetAllShops();

        /// <summary>
        /// Adding a new Shop to the Db.
        /// </summary>
        /// <param name="name">Name of the Shop.</param>
        /// <param name="bossName">Name of the manager.</param>
        /// <param name="city">Shop's city.</param>
        /// <param name="openingYear">Year the shop opened.</param>
        /// <param name="openingMonth">Month the shop opened.</param>
        /// <param name="openingDay">Day the shop opened.</param>
        /// <returns>A bool whether the insert was successful or not.</returns>
        public bool AddShop(string name, string bossName, string city, int openingYear, int openingMonth, int openingDay);

        /// <summary>
        /// Remoing a Shop from the Db.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool RemoveShop(int id);

        /// <summary>
        /// Updating a Shop with data.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <param name="data">Data we upload.</param>
        /// <returns>Whether the update was successful or not.</returns>
        bool ShopUpdate(int id, string[] data);
    }
}
