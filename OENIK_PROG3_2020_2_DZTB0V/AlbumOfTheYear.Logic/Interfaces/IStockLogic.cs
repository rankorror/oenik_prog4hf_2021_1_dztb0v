﻿// <copyright file="IStockLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The interface where we give the methods.
    /// </summary>
    public interface IStockLogic
    {
        /// <summary>
        /// Picking the stock by two ids.
        /// </summary>
        /// <param name="id">The id of the album.</param>
        /// <param name="id2">The id of the shop.</param>
        /// <returns>Returns a Stock instance.</returns>
        Stock GetStockByIds(int id, int id2);

        /// <summary>
        /// Make a list of all the stocks.
        /// </summary>
        /// <returns>A list of the stocks.</returns>
        IList<Stock> GetAllStocks();

        /// <summary>
        /// Adding a Stock (new albums hs arrived to the shop's stock.).
        /// </summary>
        /// <param name="albumId">Album of this Stock.</param>
        /// <param name="shopId">Shop where we keep this Stock.</param>
        /// <param name="qt">The number of albums we would like to keep in this Stock.</param>
        /// <returns>A bool whether the insert was successful or not.</returns>
        bool AddStock(int albumId, int shopId, int qt);

        /// <summary>
        /// Removing a Stock.
        /// </summary>
        /// <param name="id">Album's id.</param>
        /// <param name="id2">Shop's id.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool RemoveStock(int id, int id2);

        /// <summary>
        /// Updating a Stock.
        /// </summary>
        /// <param name="id">Album's id.</param>
        /// <param name="id2">Shop's id.</param>
        /// <param name="quantity">The new number of the quantity.</param>
        /// <returns>Whether the update was successful or not.</returns>
        bool StockUpdate(int id, int id2, int quantity);
    }
}
