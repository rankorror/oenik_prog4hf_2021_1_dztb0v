﻿// <copyright file="ISumChecker.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Logic.Results;

    /// <summary>
    /// Interface for the Checker logic class.
    /// </summary>
    public interface ISumChecker
    {
        /// <summary>
        /// Giving back a list of StockSumResults that shows the quantity of albums in each shop.
        /// </summary>
        /// <returns>A list of StockSumResults that shows the quantity of albums in each shop.</returns>
        IList<StockSumResults> GetStockSums();

        /// <summary>
        /// Giving back a list of CustomerSumResults that shows the number of orders for each customer.
        /// </summary>
        /// <returns>A list of CustomerSumResults that shows the number of orders for each customer.</returns>
        IList<CustomerSumResults> GetCustomerSums();

        /// <summary>
        /// Giving back a list of AlbumInShopsResults that shows the number of shops where the given album is currently in stock and the number of instances of the given album that are being sold.
        /// </summary>
        /// <returns>A list of AlbumInShopsResults that shows the number of shops where the given album is currently in stock and the number of instances of the given album that are being sold.</returns>
        IList<AlbumInShopsResults> GetAlbumInShopSums();
    }
}
