﻿// <copyright file="IOrderLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data.Model;

    /// <summary>
    /// Logic interface of Order.
    /// </summary>
    public interface IOrderLogic
    {
        /// <summary>
        /// Make a list of all the orders.
        /// </summary>
        /// <returns>A list of all the orders.</returns>
        IList<Order> GetAllOrders();

        /// <summary>
        /// Picking the order by it's id.
        /// </summary>
        /// <param name="id">Id of the chosen order.</param>
        /// <returns>An Order instance.</returns>
        Order GetOrderById(int id);

        /// <summary>
        /// Adding a new Order to the Db, ORDERING BY A CUSTOMER.
        /// </summary>
        /// <param name="shopId">The id of the shop we order from.</param>
        /// <param name="albumId">The album we order.</param>
        /// <param name="customerId">The id of the customer. If it's not in the Db, customer needs to register (add new customer) first.</param>
        /// <returns>A bool whether the adding was successful or not.</returns>
        bool AddOrder(int shopId, int albumId, int customerId);

        /// <summary>
        /// Removing an Order from the Db.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool RemoveOrder(int id);

        /// <summary>
        /// Updating an Order.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <param name="data">Data we upload.</param>
        /// <returns>Whether the update was successful or not.</returns>
        bool OrderUpdate(int id, int[] data);
    }
}
