﻿// <copyright file="IAlbumLogic.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The interface where we give the methods.
    /// </summary>
    public interface IAlbumLogic
    {
        /// <summary>
        /// Picking the album by it's id.
        /// </summary>
        /// <param name="id">Id of the chosen album.</param>
        /// <returns>An Album instance.</returns>
        Album GetAlbumById(int id);

        /// <summary>
        /// Make a list of all the albums.
        /// </summary>
        /// <returns>A list of all the albums.</returns>
        IList<Album> GetAllAlbums();

        /// <summary>
        /// Adding an album to the Db.
        /// </summary>
        /// <param name="title">Title of the album.</param>
        /// <param name="genre">Genre of the album.</param>
        /// <param name="performer">Performer of the album.</param>
        /// <param name="releaseYear">The year this album has been released.</param>
        /// <param name="isReissue">A bool that tells whether the album is a reissue edition or not.</param>
        /// <returns>A bool whether the insert was successful or not.</returns>
        bool AddAlbum(string title, string genre, string performer, int releaseYear, bool isReissue);

        /// <summary>
        /// Removing an Album instance by id.
        /// </summary>
        /// <param name="id">Id of the Album.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool RemoveAlbum(int id);

        /// <summary>
        /// Updating an Album with given data.
        /// </summary>
        /// <param name="id">Id of the album.</param>
        /// <param name="data">Data we upload.</param>
        /// <returns>Whether the update was successful or not.</returns>
        bool UpdateAlbum(int id, string[] data);
    }
}
