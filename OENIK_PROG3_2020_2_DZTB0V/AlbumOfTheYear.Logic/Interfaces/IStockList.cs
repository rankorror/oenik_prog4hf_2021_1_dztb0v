﻿// <copyright file="IStockList.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Logic.Results;

    /// <summary>
    /// Interface for the PurchaserLogic class.
    /// </summary>
    public interface IStockList
    {
        /// <summary>
        /// Gets a list of albums that are in the stock of a given shop.
        /// </summary>
        /// <param name="id">The id of the shop.</param>
        /// <returns>A List of StockList that consist albums.</returns>
        IList<StockListResults> GetStockList(int id);
    }
}
