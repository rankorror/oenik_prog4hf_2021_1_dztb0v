﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2201:Do not raise reserved exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Logic.ManagementLogic.UpdateAlbum(System.Int32,System.String[])~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2201:Do not raise reserved exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Logic.ManagementLogic.StockUpdate(System.Int32,System.Int32,System.Int32)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2201:Do not raise reserved exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Logic.ManagementLogic.ShopUpdate(System.Int32,System.String[])~System.Boolean")]
