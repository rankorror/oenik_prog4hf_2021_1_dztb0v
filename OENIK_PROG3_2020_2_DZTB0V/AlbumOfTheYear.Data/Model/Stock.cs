﻿// <copyright file="Stock.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;

    /// <summary>
    /// This table contains the stock of albums of the shops.
    /// </summary>
    [Table("Stocks")]
    public class Stock
    {
        /// <summary>
        /// Gets or sets the number of albums in a shop's stock.
        /// </summary>
        public int Quantity { get; set; }

        // FOREIGN KEYS

        /// <summary>
        /// Gets or sets the AlbumId foreign key.
        /// </summary>
        [ForeignKey(nameof(Album))]
        [Key]
        [Required]
        [NotNull]
        public int AlbumId { get; set; }

        /// <summary>
        /// Gets or sets the ShopId foreign key.
        /// </summary>
        [ForeignKey(nameof(Shop))]
        [Key]
        [Required]
        [NotNull]
        public int ShopId { get; set; }

        // NOT MAPPED PROPS

        /// <summary>
        /// Gets or sets the virtual Album.
        /// </summary>
        [NotMapped]
        public virtual Album Album { get; set; }

        /// <summary>
        /// Gets or sets the virtual Shop.
        /// </summary>
        [NotMapped]
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// Presenting the data in this instance.
        /// </summary>
        /// <returns>A string of data of this instnce.</returns>
        public string MainData()
        {
            return "Shop id: " + this.ShopId + " Album id: " + this.AlbumId + " Quantity: " + this.Quantity;
        }
    }
}
