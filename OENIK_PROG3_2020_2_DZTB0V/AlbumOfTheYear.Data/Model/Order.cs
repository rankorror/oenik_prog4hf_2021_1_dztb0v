﻿// <copyright file="Order.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;

    /// <summary>
    /// This table contains the order details.
    /// </summary>
    [Table("Orders")]
    public class Order
    {
        /// <summary>
        /// Gets or sets the unique identification of orders.
        /// </summary>
        [Key]
        [MaxLength(4)]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the date of the order.
        /// </summary>
        [Required]
        public DateTime OrderDate { get; set; }

        // FOREIGN KEYS

        /// <summary>
        /// Gets or sets the AlbumId foreign key.
        /// </summary>
        [ForeignKey(nameof(Album))]
        [Required]
        [NotNull]
        public int AlbumId { get; set; }

        /// <summary>
        /// Gets or sets the ShopId foreign key.
        /// </summary>
        [ForeignKey(nameof(Shop))]
        [Required]
        [NotNull]
        public int ShopId { get; set; }

        /// <summary>
        /// Gets or sets the id of a customer.
        /// </summary>
        [ForeignKey(nameof(Customer))]
        [Required]
        [NotNull]
        public int CustomerId { get; set; }

        // NOT MAPPED PROPS

        /// <summary>
        /// Gets or sets the virtual Album.
        /// </summary>
        [NotMapped]
        public virtual Album Album { get; set; }

        /// <summary>
        /// Gets or sets the virtual Shop.
        /// </summary>
        [NotMapped]
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// Gets or sets the virtual Customer.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Presenting the data of this order.
        /// </summary>
        /// <returns>A string that contains all the information of the order.</returns>
        public string MainData()
        {
            return " Order Id: " + this.OrderId + "Id of the Album: " + this.AlbumId + " Id of the Shop: " + this.ShopId + " Customer id: " + this.CustomerId + " Order date: " + this.OrderDate;
        }
    }
}
