﻿// A // <copyright file="Customer.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using AlbumOfTheYear.Data.Model;

    /// <summary>
    /// This is the customer table.
    /// </summary>
    [Table("Customers")]
    public class Customer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        public Customer()
        {
            this.Orders = new HashSet<Order>();
        }

        /// <summary>
        /// Gets or sets the id of a customer.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the birthdate of the customer.
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Gets or sets the email of the customer.
        /// </summary>
        [MaxLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the address of the customer.
        /// </summary>
        [MaxLength(100)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the customer.
        /// </summary>
        [MaxLength(14)]
        public string PhoneNumber { get; set; }

        // NOT MAPPED PROP

        /// <summary>
        /// Gets the list of orders where the album was ordered.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Presenting the data of this customer.
        /// </summary>
        /// <returns>A string that contains all the information about the customer instance.</returns>
        public string MainData()
        {
            return "Id: " + this.CustomerId + " Name: " + this.Name + " Address: " + this.Address + " Phone number: " + this.PhoneNumber + " Email: " + this.Email + " Birthdate: " + this.Birthdate.ToShortDateString();
        }
    }
}
