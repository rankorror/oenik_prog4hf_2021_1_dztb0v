﻿// <copyright file="Shop.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;
    using AlbumOfTheYear.Data.Model;

    /// <summary>
    /// This is the shop table.
    /// </summary>
    [Table("Shops")]
    public class Shop
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shop"/> class.
        /// </summary>
        public Shop()
        {
            this.Orders = new HashSet<Order>();
            this.Stocks = new HashSet<Stock>();
        }

        /// <summary>
        /// Gets or sets the id of the shop.
        /// </summary>
        [Key]
        [NotNull]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShopId { get; set; }

        /// <summary>
        /// Gets or sets the name of the shop.
        /// </summary>
        [MaxLength(50)]
        [Required]
        [NotNull]
        public string ShopName { get; set; }

        /// <summary>
        /// Gets or sets the name of the shop manager.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [NotNull]
        public string ManagerName { get; set; }

        /// <summary>
        /// Gets or sets the city of the shop.
        /// </summary>
        [Required]
        [NotNull]
        [MaxLength(100)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the opening date of the shop.
        /// </summary>
        [Required]
        [NotNull]
        public DateTime OpeningDate { get; set; }

        /// <summary>
        /// Gets the icollection of orders.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Gets the icollection of stocks.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Stock> Stocks { get; }

        /// <summary>
        /// Presenting the data in this shop.
        /// </summary>
        /// <returns>A string that shows the data of this shop.</returns>
        public string MainData()
        {
            return "Id: " + this.ShopId + " Shop name: " + this.ShopName + " Manager name: " + this.ManagerName + " City: " + this.City + " Opening date: " + this.OpeningDate.ToShortDateString();
        }

        /// <summary>
        /// Giving back the shop's name.
        /// </summary>
        /// <returns>The shops's name.</returns>
        public override string ToString()
        {
            return this.ShopName;
        }
    }
}
