﻿// <copyright file="Album.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;
    using AlbumOfTheYear.Data.Model;
    using Microsoft.VisualBasic.CompilerServices;

    /// <summary>
    /// This table contains the ever existing albums that can be sold online.
    /// </summary>
    [Table("Albums")]
    public class Album
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Album"/> class.
        /// </summary>
        public Album()
        {
            this.Orders = new HashSet<Order>();
            this.Stocks = new HashSet<Stock>();
        }

        /// <summary>
        /// Gets or sets a unique identification number of the album.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [NotNull]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title of the album.
        /// </summary>
        [MaxLength(200)]
        [Required]
        [NotNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the style/genre of the album.
        /// </summary>
        [MaxLength(50)]
        [Required]
        [NotNull]
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets the band/solo artist who made the album.
        /// </summary>
        [MaxLength(150)]
        [Required]
        [NotNull]
        public string Performer { get; set; }

        /// <summary>
        /// Gets or sets the year the album was released.
        /// </summary>
        [MaxLength(4)]
        [Required]
        [NotNull]
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the album is reissue or not.
        /// </summary>
        [Required]
        [NotNull]
        public bool IsReissue { get; set; }

        /// <summary>
        /// Gets the list of stocks where the album is kept.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Stock> Stocks { get; }

        /// <summary>
        /// Gets the list of orders where the album was ordered.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Presenting the data in this Album.
        /// </summary>
        /// <returns>A nicely formatted string of the data of this album.</returns>
        public string MainData()
        {
            return "Id: " + this.Id + " Performer: " + this.Performer + " Title: " + this.Title + " Release Year: " + this.ReleaseYear + " Genre: " + this.Genre + " is Reissue: " + this.IsReissue;
        }
    }
}