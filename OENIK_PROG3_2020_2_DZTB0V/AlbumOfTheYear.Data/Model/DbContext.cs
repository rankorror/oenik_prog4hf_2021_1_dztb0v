﻿// <copyright file="DbContext.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using AlbumOfTheYear.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the context thing.
    /// </summary>
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbContext"/> class.
        /// </summary>
        public DbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the DbSet of albums.
        /// </summary>
        public virtual DbSet<Album> Albums { get; set; }

        /// <summary>
        /// Gets or sets the DbSet of shops.
        /// </summary>
        public virtual DbSet<Shop> Shops { get; set; }

        /// <summary>
        /// Gets or sets the DbSet of customers.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the DbSet of stocks.
        /// </summary>
        public virtual DbSet<Stock> Stocks { get; set; }

        /// <summary>
        /// Gets or sets the DbSet of orders.
        /// </summary>
        public virtual DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Gets or sets the format provider instance.
        /// </summary>
        public IFormatProvider Ifp { get; set; }

        /// <summary>
        /// Configuring.
        /// </summary>
        /// <param name="optionsBuilder">Optionbuilder parameter.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\AOTYDb.mdf; Integrated Security=True; MultipleActiveResultSets = true;");
            }
        }

        /// <summary>
        /// Inserting the data and making connections.
        /// </summary>
        /// <param name="modelBuilder">Using modelbuilder to make connections.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Stock>()
                .HasKey(c => new { c.AlbumId, c.ShopId });

            modelBuilder.Entity<Album>()
                .HasKey(a => a.Id);

            modelBuilder.Entity<Customer>()
                .HasKey(a => a.CustomerId);

            modelBuilder.Entity<Shop>()
                .HasKey(a => a.ShopId);

            modelBuilder.Entity<Order>()
                .HasKey(a => a.OrderId);

            modelBuilder.Entity<Stock>()
                .HasOne(s => s.Album)
                .WithMany(a => a.Stocks)
                .HasForeignKey(s => s.AlbumId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Stock>()
                .HasOne(s => s.Shop)
                .WithMany(h => h.Stocks)
                .HasForeignKey(s => s.ShopId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Album)
                .WithMany(a => a.Orders)
                .HasForeignKey(o => o.AlbumId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Shop)
                .WithMany(s => s.Orders)
                .HasForeignKey(o => o.ShopId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Customer)
                .WithMany(c => c.Orders)
                .HasForeignKey(o => o.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            Album a1 = new Album() { Id = 2, Title = "Everything You've Come to Expect", Genre = "indie rock", Performer = "The Last Shadow Puppets", ReleaseYear = 2016, IsReissue = false };
            Album a2 = new Album() { Id = 3, Title = "To Be Kind", Genre = "experimental rock", Performer = "Swans", ReleaseYear = 2014, IsReissue = false };
            Album a3 = new Album() { Id = 4, Title = "Container Ships", Genre = "sludge metal", Performer = "Kowloon Walled City", ReleaseYear = 2012, IsReissue = false };
            Album a4 = new Album() { Id = 5, Title = "Vollmilch", Genre = "funk", Performer = "Vulfpeck", ReleaseYear = 2012, IsReissue = false };
            Album a5 = new Album() { Id = 6, Title = "The Texas-Jerusalem Crossroads", Genre = "post-rock", Performer = "Lift To Experience", ReleaseYear = 2001, IsReissue = false };
            Album a6 = new Album() { Id = 7, Title = "S/T", Genre = "indie rock", Performer = "Young Jesus", ReleaseYear = 2017, IsReissue = false };
            Album a7 = new Album() { Id = 8, Title = "OK Computer", Genre = "alternative rock", Performer = "Radiohead", ReleaseYear = 1997, IsReissue = false };
            Album a8 = new Album() { Id = 9, Title = "Nevermind", Genre = "grunge", Performer = "Nirvana", ReleaseYear = 1991, IsReissue = false };
            Album a9 = new Album() { Id = 10, Title = "Nevermind", Genre = "grunge", Performer = "Nirvana", ReleaseYear = 2002, IsReissue = true };
            Album a10 = new Album() { Id = 11, Title = "Soft Opening", Genre = "indie rock", Performer = "Posse", ReleaseYear = 2014, IsReissue = false };
            Album a11 = new Album() { Id = 12, Title = "Please Be Nice", Genre = "emo", Performer = "Camping In Alaska", ReleaseYear = 2013, IsReissue = false };
            Album a12 = new Album() { Id = 13, Title = "AM", Genre = "indie rock", Performer = "Arctic Monkeys", ReleaseYear = 2013, IsReissue = false };
            Album a13 = new Album() { Id = 14, Title = "AM", Genre = "indie rock", Performer = "Arctic Monkeys", ReleaseYear = 2018, IsReissue = true };
            Album a14 = new Album() { Id = 15, Title = "Yankee Hotel Foxtrot", Genre = "alternative rock", Performer = "Wilco", ReleaseYear = 2002, IsReissue = false };
            Album a15 = new Album() { Id = 16, Title = "Electro-Shock for President", Genre = "noise-rock", Performer = "Brainiac", ReleaseYear = 1997, IsReissue = false };
            Album a16 = new Album() { Id = 17, Title = "Yank Crime", Genre = "post-hardcore", Performer = "Drive Like Jehu", ReleaseYear = 1994, IsReissue = false };
            Album a17 = new Album() { Id = 18, Title = "Tubular Bells", Genre = "progressive rock", Performer = "Mike Oldfield", ReleaseYear = 1973, IsReissue = false };
            Album a18 = new Album() { Id = 19, Title = "Slow Burn", Genre = "screamo", Performer = "Old Gray", ReleaseYear = 2016, IsReissue = false };
            Album a19 = new Album() { Id = 20, Title = "Songs About Leaving", Genre = "slowcore", Performer = "Carissa's Wierd", ReleaseYear = 2002, IsReissue = false };
            Album a20 = new Album() { Id = 21, Title = "Goo", Genre = "noise-rock", Performer = "Sonic Youth", ReleaseYear = 1990, IsReissue = false };
            Album a21 = new Album() { Id = 1, Title = "Horse Blanket", Genre = "indie rock", Performer = "Posse", ReleaseYear = 2017, IsReissue = false };

            Shop sh1 = new Shop() { ShopId = 1, ShopName = "Wave", ManagerName = "Pápai Sándor", City = "Budapest", OpeningDate = DateTime.Parse("1989.6.13", this.Ifp) };
            Shop sh2 = new Shop() { ShopId = 2, ShopName = "Lemezkuckó", ManagerName = "Bánházi István", City = "Budapest", OpeningDate = DateTime.Parse("1986.12.10", this.Ifp) };
            Shop sh3 = new Shop() { ShopId = 3, ShopName = "Laci Bácsi Lemezboltja", ManagerName = "Molnár László", City = "Budapest", OpeningDate = DateTime.Parse("1999.2.20", this.Ifp) };
            Shop sh4 = new Shop() { ShopId = 4, ShopName = "Zenemánia", ManagerName = "Tubák Csaba", City = "Pécs", OpeningDate = DateTime.Parse("1994.4.4", this.Ifp) };
            Shop sh5 = new Shop() { ShopId = 5, ShopName = "Audio", ManagerName = "Lakatos Csaba", City = "Miskolc", OpeningDate = DateTime.Parse("1984.9.16", this.Ifp) };

            Customer c1 = new Customer() { CustomerId = 1, Name = "Jakab Tamás", Birthdate = DateTime.Parse("1995.5.25", this.Ifp), Address = "Szurdokpüspöki, Gát út 30.", Email = "csokolom666@gmail.com", PhoneNumber = "+36304456923" };
            Customer c2 = new Customer() { CustomerId = 2, Name = "Fenér Hóra", Birthdate = DateTime.Parse("1998.6.22", this.Ifp), Address = "Budapest, Mozart utca 101.", Email = "noribori29@gmail.com", PhoneNumber = "+36204486923" };
            Customer c3 = new Customer() { CustomerId = 3, Name = "Leó Leó", Birthdate = DateTime.Parse("1975.1.5", this.Ifp), Address = "Szeged, Göröngy út 17.", Email = "duplaleo@gmail.com", PhoneNumber = "+36706678453" };
            Customer c4 = new Customer() { CustomerId = 4, Name = "Kelemen Péter", Birthdate = DateTime.Parse("1977.3.14", this.Ifp), Address = "Pásztó, Iglu út 31.", Email = "peter.kelcsi@gmail.com", PhoneNumber = "+36301234567" };
            Customer c5 = new Customer() { CustomerId = 5, Name = "Csank Jakab", Birthdate = DateTime.Parse("1988.1.1", this.Ifp), Address = "Salgótarján, Losonci út 10.", Email = "csanki@gmail.com", PhoneNumber = "+36203167382" };
            Customer c6 = new Customer() { CustomerId = 6, Name = "Pege Ferenc", Birthdate = DateTime.Parse("1950.12.25", this.Ifp), Address = "Budapest, Auróra utca 104.", Email = "pegeferi32@gmail.com", PhoneNumber = "+367088934562" };
            Customer c7 = new Customer() { CustomerId = 7, Name = "Bajnok Dzsenifer", Birthdate = DateTime.Parse("1980.4.25", this.Ifp), Address = "Pécs, Parmezán utca 14.", Email = "dzsnei@gmail.com", PhoneNumber = "+363020160619" };
            Customer c8 = new Customer() { CustomerId = 8, Name = "Kon Tamara", Birthdate = DateTime.Parse("2000.7.7", this.Ifp), Address = "Szombathely, Csontváz utca 42.", Email = "tami432@gmail.com", PhoneNumber = "+36203671289" };

            Stock s1 = new Stock() { AlbumId = 1, ShopId = 5, Quantity = 30 };
            Stock s2 = new Stock() { AlbumId = 3, ShopId = 5, Quantity = 40 };
            Stock s3 = new Stock() { AlbumId = 4, ShopId = 5, Quantity = 3 };
            Stock s4 = new Stock() { AlbumId = 8, ShopId = 5, Quantity = 5 };
            Stock s5 = new Stock() { AlbumId = 10, ShopId = 5, Quantity = 10 };
            Stock s6 = new Stock() { AlbumId = 11, ShopId = 5, Quantity = 14 };
            Stock s7 = new Stock() { AlbumId = 15, ShopId = 5, Quantity = 26 };
            Stock s8 = new Stock() { AlbumId = 17, ShopId = 5, Quantity = 2 };
            Stock s9 = new Stock() { AlbumId = 18, ShopId = 5, Quantity = 20 };
            Stock s10 = new Stock() { AlbumId = 19, ShopId = 5, Quantity = 10 };

            Stock s11 = new Stock() { AlbumId = 2, ShopId = 4, Quantity = 3 };
            Stock s12 = new Stock() { AlbumId = 5, ShopId = 4, Quantity = 9 };
            Stock s13 = new Stock() { AlbumId = 6, ShopId = 4, Quantity = 11 };
            Stock s14 = new Stock() { AlbumId = 7, ShopId = 4, Quantity = 15 };
            Stock s15 = new Stock() { AlbumId = 13, ShopId = 4, Quantity = 82 };
            Stock s16 = new Stock() { AlbumId = 14, ShopId = 4, Quantity = 17 };
            Stock s17 = new Stock() { AlbumId = 20, ShopId = 4, Quantity = 7 };

            Stock s18 = new Stock() { AlbumId = 1, ShopId = 3, Quantity = 10 };
            Stock s19 = new Stock() { AlbumId = 4, ShopId = 3, Quantity = 6 };
            Stock s20 = new Stock() { AlbumId = 5, ShopId = 3, Quantity = 16 };
            Stock s21 = new Stock() { AlbumId = 6, ShopId = 3, Quantity = 41 };
            Stock s22 = new Stock() { AlbumId = 10, ShopId = 3, Quantity = 10 };
            Stock s23 = new Stock() { AlbumId = 15, ShopId = 3, Quantity = 15 };
            Stock s24 = new Stock() { AlbumId = 17, ShopId = 3, Quantity = 56 };
            Stock s25 = new Stock() { AlbumId = 18, ShopId = 3, Quantity = 43 };

            Stock s26 = new Stock() { AlbumId = 3, ShopId = 2, Quantity = 8 };
            Stock s27 = new Stock() { AlbumId = 7, ShopId = 2, Quantity = 4 };
            Stock s28 = new Stock() { AlbumId = 15, ShopId = 2, Quantity = 0 };
            Stock s29 = new Stock() { AlbumId = 18, ShopId = 2, Quantity = 100 };
            Stock s30 = new Stock() { AlbumId = 19, ShopId = 2, Quantity = 59 };
            Stock s31 = new Stock() { AlbumId = 20, ShopId = 2, Quantity = 36 };

            Stock s32 = new Stock() { AlbumId = 1, ShopId = 1, Quantity = 3 };
            Stock s33 = new Stock() { AlbumId = 2, ShopId = 1, Quantity = 1 };
            Stock s34 = new Stock() { AlbumId = 3, ShopId = 1, Quantity = 5 };
            Stock s35 = new Stock() { AlbumId = 4, ShopId = 1, Quantity = 4 };
            Stock s36 = new Stock() { AlbumId = 5, ShopId = 1, Quantity = 12 };
            Stock s37 = new Stock() { AlbumId = 11, ShopId = 1, Quantity = 15 };
            Stock s38 = new Stock() { AlbumId = 12, ShopId = 1, Quantity = 20 };
            Stock s39 = new Stock() { AlbumId = 13, ShopId = 1, Quantity = 13 };
            Stock s40 = new Stock() { AlbumId = 21, ShopId = 1, Quantity = 5 };

            Order o1 = new Order() { OrderId = 1, CustomerId = 1, AlbumId = 14, ShopId = 4, OrderDate = DateTime.Parse("2020.1.13", this.Ifp) };
            Order o2 = new Order() { OrderId = 2, CustomerId = 2, AlbumId = 5, ShopId = 3, OrderDate = DateTime.Parse("2020.2.3", this.Ifp) };
            Order o3 = new Order() { OrderId = 3, CustomerId = 3, AlbumId = 2, ShopId = 1, OrderDate = DateTime.Parse("2020.2.28", this.Ifp) };
            Order o4 = new Order() { OrderId = 4, CustomerId = 4, AlbumId = 19, ShopId = 2, OrderDate = DateTime.Parse("2020.5.23", this.Ifp) };
            Order o5 = new Order() { OrderId = 5, CustomerId = 5, AlbumId = 5, ShopId = 2, OrderDate = DateTime.Parse("2020.4.14", this.Ifp) };
            Order o6 = new Order() { OrderId = 6, CustomerId = 6, AlbumId = 8, ShopId = 5, OrderDate = DateTime.Parse("2020.10.26", this.Ifp) };
            Order o7 = new Order() { OrderId = 7, CustomerId = 7, AlbumId = 9, ShopId = 5, OrderDate = DateTime.Parse("2020.9.16", this.Ifp) };
            Order o8 = new Order() { OrderId = 8, CustomerId = 8, AlbumId = 17, ShopId = 4, OrderDate = DateTime.Parse("2020.6.16", this.Ifp) };
            Order o9 = new Order() { OrderId = 9, CustomerId = 3, AlbumId = 4, ShopId = 3, OrderDate = DateTime.Parse("2020.1.20", this.Ifp) };
            Order o10 = new Order() { OrderId = 10, CustomerId = 4, AlbumId = 21, ShopId = 1, OrderDate = DateTime.Parse("2020.4.19", this.Ifp) };
            Order o11 = new Order() { OrderId = 11, CustomerId = 7, AlbumId = 19, ShopId = 2, OrderDate = DateTime.Parse("2020.12.13", this.Ifp) };

            modelBuilder.Entity<Album>()
               .HasData(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21);

            modelBuilder.Entity<Shop>()
                .HasData(sh1, sh2, sh3, sh4, sh5);

            modelBuilder.Entity<Customer>()
                .HasData(c1, c2, c3, c4, c5, c6, c7, c8);

            modelBuilder.Entity<Order>()
                .HasData(o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11);

            modelBuilder.Entity<Stock>()
                .HasData(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20, s21, s22, s23, s24, s25, s26, s27, s28, s29, s30, s31, s32, s33, s34, s35, s36, s37, s38, s39, s40);
        }
    }
}
