﻿// <copyright file="GlobalSuppressions.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Data.DbContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Data.DbContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder)")]
