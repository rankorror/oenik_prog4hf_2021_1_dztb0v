﻿// <copyright file="ShopRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repo of the shop, here we define the methods.
    /// </summary>
    public class ShopRepository : Repo<Shop>, IShopRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShopRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext instance.</param>
        public ShopRepository(Microsoft.EntityFrameworkCore.DbContext ctx)
            : base(ctx)
        {
            // Nem csinál semmit.
        }

        /// <summary>
        /// Gets the IFP.
        /// </summary>
        public IFormatProvider Ifp { get; }

        /// <summary>
        /// Returns a shop by id.
        /// </summary>
        /// <param name="id">The id of the chosen shop.</param>
        /// <returns>The chosen shop.</returns>
        public override Shop GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ShopId == id);
        }

        /// <summary>
        /// Here we don't need this.
        /// </summary>
        /// <param name="id">XYZ.</param>
        /// <param name="id2">XYZ2.</param>
        /// <returns>Here, we return nothing.</returns>
        public override Shop GetOne(int id, int id2)
        {
            // DO NOTHING
            return null;
        }

        /// <summary>
        /// Adding a Shop instance.
        /// </summary>
        /// <param name="entity">The Shop we add.</param>
        public override void Insert(Shop entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Removing a Shop by Id.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <returns>A Shop instance.</returns>
        public override bool Remove(int id)
        {
            if (this.Ctx.Set<Shop>().Contains(this.GetOne(id)))
            {
                this.Ctx.Remove(this.GetOne(id));
                this.Ctx.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updating the Shop.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <param name="data">The data we upload.</param>
        public void UpdateShop(int id, string[] data)
        {
            // name, managername, city, date
            var sh = this.GetOne(id);
            sh.ShopName = data[0];
            sh.ManagerName = data[1];
            sh.City = data[2];
            sh.OpeningDate = DateTime.Parse(int.Parse(data[3], this.Ifp) + "." + int.Parse(data[4], this.Ifp) + "." + int.Parse(data[5], this.Ifp), this.Ifp);

            this.Ctx.SaveChanges();
        }
    }
}
