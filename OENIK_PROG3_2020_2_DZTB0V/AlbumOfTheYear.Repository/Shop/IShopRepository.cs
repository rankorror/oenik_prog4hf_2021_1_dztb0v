﻿// <copyright file="IShopRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The specific, inherited Shoprepo interface where the methods are "prefixed".
    /// </summary>
    public interface IShopRepository : IRepository<Shop>
    {
        /// <summary>
        /// Updating a Shop instance.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <param name="data">Data we upload.</param>
        public void UpdateShop(int id, string[] data);

        /// <summary>
        /// Removing a Shop by id.
        /// </summary>
        /// <param name="id">Id of the Shop.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        new bool Remove(int id);
    }
}
