﻿// <copyright file="IOrderRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data.Model;

    /// <summary>
    /// The specific, inherited Orderrepo interface where the methods are "prefixed".
    /// </summary>
    public interface IOrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Updating an Order instance.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <param name="data">The data we upload.</param>
        public void OrderUpdate(int id, int[] data);

        /// <summary>
        /// Removing an Order from the Db.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        new bool Remove(int id);
    }
}
