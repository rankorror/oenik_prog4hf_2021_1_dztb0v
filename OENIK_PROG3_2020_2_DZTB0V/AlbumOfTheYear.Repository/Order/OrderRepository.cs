﻿// <copyright file="OrderRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repo of the order, here we define the methods.
    /// </summary>
    public class OrderRepository : Repo<Order>, IOrderRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext instance.</param>
        public OrderRepository(DbContext ctx)
            : base(ctx)
        {
            // Nem csinál semmit.
        }

        /// <summary>
        /// Returns an order by id.
        /// </summary>
        /// <param name="id">The id of the chosen order.</param>
        /// <returns>The chosen order.</returns>
        public override Order GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.OrderId == id);
        }

        /// <summary>
        /// We can't use this method with orderrepo.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="id2">Another Id.</param>
        /// <returns>Returns null in this case.</returns>
        public override Order GetOne(int id, int id2)
        {
            return null;
        }

        /// <summary>
        /// Adding a new Order instance.
        /// </summary>
        /// <param name="entity">The Order we insert.</param>
        public override void Insert(Order entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Updating an Order instance.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <param name="data">The data we upload.</param>
        public void OrderUpdate(int id, int[] data)
        {
            // sorrend:albumid, shopid, customerid
            var ord = this.GetOne(id);
            ord.AlbumId = data[0];
            ord.ShopId = data[1];
            ord.CustomerId = data[2];

            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Removing an Order.
        /// </summary>
        /// <param name="id">Id of the Order.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        public override bool Remove(int id)
        {
            if (this.Ctx.Set<Order>().Contains(this.GetOne(id)))
            {
                this.Ctx.Remove(this.GetOne(id));
                this.Ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
