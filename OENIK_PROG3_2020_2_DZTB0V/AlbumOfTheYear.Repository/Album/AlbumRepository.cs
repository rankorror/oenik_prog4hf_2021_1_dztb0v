﻿// <copyright file="AlbumRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repo of the album, here we define the methods.
    /// </summary>
    public class AlbumRepository : Repo<Album>, IAlbumRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumRepository"/> class.
        /// </summary>
        /// <param name="ctx">The Dbcontext instance.</param>
        public AlbumRepository(Microsoft.EntityFrameworkCore.DbContext ctx)
            : base(ctx)
        {
            // Nem csinál semmit.
        }

        /// <summary>
        /// Gets or sets an IFormatProvider instance.
        /// </summary>
        public IFormatProvider Ifp { get; set; }

        /// <summary>
        /// Updating an existing Album instance.
        /// </summary>
        /// <param name="id">Id of the album.</param>
        /// <param name="data">The properties of the album instance.</param>
        public void AlbumUpdate(int id, string[] data)
        {
            // sorrend : cím, performer, genre, release year, reissueness
            var album = this.GetOne(id);
            album.Title = data[0];
            album.Performer = data[1];
            album.Genre = data[2];
            album.ReleaseYear = int.Parse(data[3], this.Ifp);
            if (data[4] == "True")
            {
                album.IsReissue = true;
            }
            else
            {
                album.IsReissue = false;
            }

            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Returns an album by id.
        /// </summary>
        /// <param name="id">The id of the chosen album.</param>
        /// <returns>The chosen album.</returns>
        public override Album GetOne(int id)
        {
            var list = this.Ctx.Set<Album>().ToList();

            if (list.Any(x => x.Id == id))
            {
                return this.GetAll().SingleOrDefault(x => x.Id == id);
            }

            return null;
        }

        /// <summary>
        /// We can't use this method with albumrepo.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="id2">Another Id.</param>
        /// <returns>Returns null in this case.</returns>
        public override Album GetOne(int id, int id2)
        {
            return null;
        }

        /// <summary>
        /// Inserting an Album instance into the Db.
        /// </summary>
        /// <param name="entity">The Album to insert.</param>
        public override void Insert(Album entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Removing an album instance from the Db by it's id.
        /// </summary>
        /// <param name="id">Id of the album to remove.</param>
        /// <returns>A bool whether the removing was successful or not.</returns>
        public override bool Remove(int id)
        {
            if (this.Ctx.Set<Album>().Contains(this.GetOne(id)))
            {
                this.Ctx.Remove(this.GetOne(id));
                this.Ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
