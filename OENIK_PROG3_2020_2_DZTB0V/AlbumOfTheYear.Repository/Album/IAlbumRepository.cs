﻿// <copyright file="IAlbumRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The specific, inherited Albumrepo interface where the methods are "prefixed".
    /// </summary>
    public interface IAlbumRepository : IRepository<Album>
    {
        /// <summary>
        /// Updating an Album.
        /// </summary>
        /// <param name="id">Id of the album.</param>
        /// <param name="data">The data we upload.</param>
        public void AlbumUpdate(int id, string[] data);

        /// <summary>
        /// Removing an album by id.
        /// </summary>
        /// <param name="id">Id of the Album we remove.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        new bool Remove(int id);
    }
}
