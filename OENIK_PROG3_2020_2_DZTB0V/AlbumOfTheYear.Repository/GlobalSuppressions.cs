﻿// <copyright file="GlobalSuppressions.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Repository.OrderRepository.OrderUpdate(System.Int32,System.Int32[])")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Repository.AlbumRepository.AlbumUpdate(System.Int32,System.String[])")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Repository.CustomerRepository.CustomerUpdate(System.Int32,System.String[])")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:AlbumOfTheYear.Repository.ShopRepository.UpdateShop(System.Int32,System.String[])")]
