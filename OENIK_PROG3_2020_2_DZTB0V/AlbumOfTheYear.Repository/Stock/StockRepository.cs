﻿// <copyright file="StockRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repo of the stock, here we define the methods.
    /// </summary>
    public class StockRepository : Repo<Stock>, IStockRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StockRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext instance.</param>
        public StockRepository(Microsoft.EntityFrameworkCore.DbContext ctx)
            : base(ctx)
        {
            // Nem csinál semmit.
        }

        /// <summary>
        /// Changing the number of albums in a shop's stock.
        /// </summary>
        /// <param name="albumId">The id of the chosen album.</param>
        /// <param name="shopId">The id of the chosen shop.</param>
        /// <param name="newQuantity">The new quantity of the album in the shop's stock.</param>
        public void UpdateStock(int albumId, int shopId, int newQuantity)
        {
            var stock = this.GetOne(albumId, shopId);
            stock.Quantity = newQuantity;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Returns a stock instance of the given ids.
        /// </summary>
        /// <param name="id">The id of the album of this stock.</param>
        /// <param name="id2">The id of the shop of this stock.</param>
        /// <returns>A stock instance.</returns>
        public override Stock GetOne(int id, int id2)
        {
            var list = this.Ctx.Set<Stock>().ToList();

            if (list.Where(x => x.AlbumId == id).Any(x => x.ShopId == id2))
            {
                    IQueryable<Stock> firstId = this.GetAll().Where(x => x.AlbumId == id);
                    return firstId.SingleOrDefault(y => y.ShopId == id2);
            }

            return null;
        }

        /// <summary>
        /// We don't need this here.
        /// </summary>
        /// <param name="id">XYZ.</param>
        /// <returns>This returns null here.</returns>
        public override Stock GetOne(int id)
        {
            return null;
        }

        /// <summary>
        /// Removing a Stock by it's two id.
        /// </summary>
        /// <param name="id">Album id of this Stock.</param>
        /// <param name="id2">Shop id of this Stock.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        public bool Remove(int id, int id2)
        {
            if (this.GetOne(id, id2) != null)
            {
                this.Ctx.Remove(this.GetOne(id, id2));
                this.Ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Inserting a Srock instance.
        /// </summary>
        /// <param name="entity">Stock to insert.</param>
        public override void Insert(Stock entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// We can't remove a Stock by only one id.
        /// </summary>
        /// <param name="id">An id.</param>
        /// <returns>FALSE.</returns>
        public override bool Remove(int id)
        {
            // WE USE THE 2 ID METHOD TO REMOVE STOCK.
            return false;
        }
    }
}
