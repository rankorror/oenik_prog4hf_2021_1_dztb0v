﻿// <copyright file="IStockRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The specific, inherited Stockrepo interface where the methods are "prefixed".
    /// </summary>
    public interface IStockRepository : IRepository<Stock>
    {
        /// <summary>
        /// Changing the number of albums in a shop's stock.
        /// </summary>
        /// <param name="albumId">The id of the chosen album.</param>
        /// <param name="shopId">The id of the chosen shop.</param>
        /// <param name="newQuantity">The new number of the stock.</param>
        void UpdateStock(int albumId, int shopId, int newQuantity);

        /// <summary>
        /// Removing a Stock instance by it's two ids.
        /// </summary>
        /// <param name="id">Album id of this Stock.</param>
        /// <param name="id2">Shop id of this Stock.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool Remove(int id, int id2);
    }
}
