﻿// <copyright file="CustomerRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AlbumOfTheYear.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repo of the album, here we define the methods.
    /// </summary>
    public class CustomerRepository : Repo<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext instance.</param>
        public CustomerRepository(Microsoft.EntityFrameworkCore.DbContext ctx)
            : base(ctx)
        {
            // Nem csinál semmit.
        }

        /// <summary>
        /// Gets the IFormatProvider instance.
        /// </summary>
        public IFormatProvider Ifp { get; }

        /// <summary>
        /// Updating the existing Customer instance.
        /// </summary>
        /// <param name="id">Id of the customer.</param>
        /// <param name="data">The data we upload.</param>
        /// <returns>Whether the update was successful or not.</returns>
        public bool CustomerUpdate(int id, string[] data)
        {
            // sorrend: név, születési idő(1-3), email, cím, telefonszám
            var customer = this.GetOne(id);
            customer.Name = data[0];
            customer.Birthdate = DateTime.Parse(int.Parse(data[1], this.Ifp) + "." + int.Parse(data[2], this.Ifp) + "." + int.Parse(data[3], this.Ifp), this.Ifp);
            customer.Email = data[4];
            customer.Address = data[5];
            customer.PhoneNumber = data[6];

            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Returns a customer by id.
        /// </summary>
        /// <param name="id">The id of the chosen customer.</param>
        /// <returns>The customer we neeeeed.</returns>
        public override Customer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CustomerId == id);
        }

        /// <summary>
        /// We don't need this here.
        /// </summary>
        /// <param name="id">XYZ.</param>
        /// <param name="id2">XYZ2.</param>
        /// <returns>Here, it return null.</returns>
        public override Customer GetOne(int id, int id2)
        {
            return null;
        }

        /// <summary>
        /// Addin a new Customer to the Db.
        /// </summary>
        /// <param name="entity">The Customer we add.</param>
        public override void Insert(Customer entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Removing a Customer by id.
        /// </summary>
        /// <param name="id">Id of the Customer.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        public override bool Remove(int id)
        {
            if (this.Ctx.Set<Customer>().Contains(this.GetOne(id)))
            {
                this.Ctx.Remove(this.GetOne(id));
                this.Ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
