﻿// <copyright file="ICustomerRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AlbumOfTheYear.Data;

    /// <summary>
    /// The specific, inherited Customerrepo interface where the methods are "prefixed".
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Updating the customer with data.
        /// </summary>
        /// <param name="id">Id of the Customer instance.</param>
        /// <param name="data">The data we upload.</param>
        /// <returns>A bool whether the update was successful or not.</returns>
        public bool CustomerUpdate(int id, string[] data);

        /// <summary>
        /// Removing a Customer by id.
        /// </summary>
        /// <param name="id">Id of the Customer.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        new bool Remove(int id);
    }
}
