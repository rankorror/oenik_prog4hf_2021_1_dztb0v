﻿// <copyright file="Repo.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repo class.
    /// </summary>
    /// <typeparam name="T">T generic class.</typeparam>
    public abstract class Repo<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repo{T}"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext instance.</param>
        protected Repo(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets the DbContext we use.
        /// </summary>
        public DbContext Ctx { get; set; }

        /// <summary>
        /// Gets the set of T generic class instances.
        /// </summary>
        /// <returns>A set of T generic class instances.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>(); // set => halmaz.
        }

        /// <summary>
        /// Gets a T typed class instance by it's id.
        /// </summary>
        /// <param name="id">Id of the instance we search for.</param>
        /// <returns>A T typed instance.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Gets a T typed instance by two ids.
        /// </summary>
        /// <param name="id">First identification number.</param>
        /// <param name="id2">Second identification number.</param>
        /// <returns>A T typed instance.</returns>
        public abstract T GetOne(int id, int id2);

        /// <summary>
        /// Inserting a new instance.
        /// </summary>
        /// <param name="entity">The instance we insert.</param>
        public abstract void Insert(T entity);

        /// <summary>
        /// Removing a new instance by id.
        /// </summary>
        /// <param name="id">Id of the instance.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        public abstract bool Remove(int id);
    }
}
