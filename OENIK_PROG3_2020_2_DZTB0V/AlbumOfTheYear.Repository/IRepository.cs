﻿// <copyright file="IRepository.cs" company="AlbumOfTheYear">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// T generic interface to inherit.
    /// </summary>
    /// <typeparam name="T">Generic type.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets a T typed class instance.
        /// </summary>
        /// <param name="id">The id of the chosen class instance.</param>
        /// <returns>An instance of a class.</returns>
        T GetOne(int id);

        // A GETONE ÉS A REMOVE KÉTPARAMÉTERES VERZIÓIT LEHET ÉRDEMES LENNE KÜLÖN A SEPCIFIKÁLT REPOKBA KIÍRNI. (STOCK)

        /// <summary>
        /// Gets a T typed class instance by two ids.
        /// </summary>
        /// <param name="id">The id of the chosen class instance.</param>
        /// <param name="id2">The other id that helps to identify the record.</param>
        /// <returns>An instance of a class.</returns>
        T GetOne(int id, int id2);

        /// <summary>
        /// Returns a list of T typed classes instances.
        /// </summary>
        /// <returns>A list of instances of a class.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Adding a new instance.
        /// </summary>
        /// <param name="entity">The new instance.</param>
        void Insert(T entity);

        /// <summary>
        /// Removing an instance by id.
        /// </summary>
        /// <param name="id">The id of the instance in the Db.</param>
        /// <returns>A bool whether the remove was successful or not.</returns>
        bool Remove(int id);
    }
}
