﻿// <copyright file="AlbumsApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using AlbumOfTheYear.Logic;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller for API.
    /// </summary>
    public class AlbumsApiController : Controller
    {
        private ApiResult result;
        private IAlbumLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumsApiController"/> class.
        /// </summary>
        /// <param name="logic">Logic class.</param>
        /// <param name="mapper">Automapper instance.</param>
        public AlbumsApiController(IAlbumLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
            this.result = new ApiResult();
        }

        /// <summary>
        /// Gets all the albums in one list.
        /// </summary>
        /// <returns>A list consisting all of the albums.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Album> GetAll()
        {
            var albumshere = this.logic.GetAllAlbums();
            return this.mapper.Map<IList<Data.Album>, List<Models.Album>>(albumshere);
        }

        /// <summary>
        /// Deleting one album from the database.
        /// </summary>
        /// <param name="id">The id of the album to delete.</param>
        /// <returns>A result whether the delete was successful or not.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneAlbum(int id)
        {
            this.result.OperationResult = this.logic.RemoveAlbum(id);
            return this.result;
        }

        /// <summary>
        /// Adding an album to the database.
        /// </summary>
        /// <param name="a">The album to add.</param>
        /// <returns>A result whether the adding method was successful or not.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneAlbum(Models.Album a)
        {
            bool success = true;

            try
            {
                this.logic.AddAlbum(a.Title, a.Genre, a.Performer, a.ReleaseYear, a.IsReissue);
                success = true;
            }
            catch (ArgumentException)
            {
                success = false;
            }

            this.result.OperationResult = success;
            return this.result;
        }

        /// <summary>
        /// Modifying an album.
        /// </summary>
        /// <param name="a">The album to modify.</param>
        /// <returns>A result whether the modifying method was successful or not.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneAlbum(Models.Album a)
        {
            this.result.OperationResult = this.logic.UpdateAlbum(a.Id, new string[] { a.Title, a.Performer, a.Genre, a.ReleaseYear.ToString(CultureInfo.CurrentCulture), a.IsReissue.ToString() });
            return this.result;
        }
    }
}
