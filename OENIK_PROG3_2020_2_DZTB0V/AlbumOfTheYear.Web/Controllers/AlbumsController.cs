﻿// <copyright file="AlbumsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AlbumOfTheYear.Logic;
    using AlbumOfTheYear.Web.Models;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller of the WebApplication.
    /// </summary>
    public class AlbumsController : Controller
    {
        private IAlbumLogic logic;
        private IMapper mapper;
        private AlbumListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumsController"/> class.
        /// </summary>
        /// <param name="logic">Logic interface.</param>
        /// <param name="mapper">Mapper interface.</param>
        public AlbumsController(IAlbumLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new AlbumListViewModel();
            this.vm.EditedAlbum = new Models.Album();

            var albums = this.logic.GetAllAlbums();
            this.vm.ListOfAlbums = this.mapper.Map<IList<Data.Album>, List<Models.Album>>(albums);
        }

        /// <summary>
        /// Gets or sets the IFP.
        /// </summary>
        public IFormatProvider IFP { get; set; }

        /// <summary>
        /// Edit Action method.
        /// </summary>
        /// <returns>Action result.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("AlbumsIndex", this.vm);
        }

        /// <summary>
        /// Showing details of the album.
        /// </summary>
        /// <param name="id">Id of the album to show.</param>
        /// <returns>Action result.</returns>
        public IActionResult Details(int id)
        {
            return this.View("AlbumsDetails", this.GetAlbum(id));
        }

        /// <summary>
        /// Removing an album.
        /// </summary>
        /// <param name="id">Id of the album to remove.</param>
        /// <returns>Action result.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.logic.RemoveAlbum(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Editing an album.
        /// </summary>
        /// <param name="id">Id of the album to edit.</param>
        /// <returns>Action result.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedAlbum = this.GetAlbum(id);
            return this.View("AlbumsIndex", this.vm);
        }

        /// <summary>
        /// Editing action.
        /// </summary>
        /// <param name="album">Album to edit.</param>
        /// <param name="editAction">Action name.</param>
        /// <returns>Action result.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Album album, string editAction)
        {
            if (this.ModelState.IsValid && album != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    this.logic.AddAlbum(album.Title, album.Genre, album.Performer, album.ReleaseYear, album.IsReissue);
                }
                else
                {
                    if (!this.logic.UpdateAlbum(album.Id, new string[] { album.Title, album.Performer, album.Genre, album.ReleaseYear.ToString(this.IFP), album.IsReissue.ToString() }))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedAlbum = album;
                return this.View("AlbumsIndex", this.vm);
            }
        }

        private Models.Album GetAlbum(int id)
        {
            Data.Album oneAlbum = this.logic.GetAlbumById(id);
            return this.mapper.Map<Data.Album, Models.Album>(oneAlbum);
        }
    }
}
