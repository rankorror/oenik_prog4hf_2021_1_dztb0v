﻿// <copyright file="MapperFacotry.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Mapper Factory class.
    /// </summary>
    public static class MapperFacotry
    {
        /// <summary>
        /// Creating the mapper from data to model.album.
        /// </summary>
        /// <returns>Mapper instance.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AlbumOfTheYear.Data.Album, AlbumOfTheYear.Web.Models.Album>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.Title, map => map.MapFrom(src => src.Title)).
                    ForMember(dest => dest.Performer, map => map.MapFrom(src => src.Performer)).
                    ForMember(dest => dest.ReleaseYear, map => map.MapFrom(src => src.ReleaseYear)).
                    ForMember(dest => dest.IsReissue, map => map.MapFrom(src => src.IsReissue)).
                    ForMember(dest => dest.Genre, map => map.MapFrom(src => src.Genre));
            });
            return config.CreateMapper();
        }
    }
}
