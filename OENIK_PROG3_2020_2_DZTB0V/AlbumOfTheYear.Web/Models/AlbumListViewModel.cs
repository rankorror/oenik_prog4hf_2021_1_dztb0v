﻿// <copyright file="AlbumListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// ViewModel to albums' list.
    /// </summary>
    public class AlbumListViewModel
    {
        /// <summary>
        /// Gets or sets the list of the albums.
        /// </summary>
        public List<Album> ListOfAlbums { get; set; }

        /// <summary>
        /// Gets or sets the album that has been edited.
        /// </summary>
        public Album EditedAlbum { get; set; }
    }
}
