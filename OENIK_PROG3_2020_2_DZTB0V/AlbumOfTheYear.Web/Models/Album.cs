﻿// <copyright file="Album.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AlbumOfTheYear.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Album Model in WebApplication.
    /// </summary>
    public class Album
    {
        /// <summary>
        /// Gets or sets the id of the album.
        /// </summary>
        [Display(Name = "Album Id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title of the album.
        /// </summary>
        [Display(Name = "Album Title")]
        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the genre of the album.
        /// </summary>
        [Display(Name = "Album Genre")]
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets the performer of the album.
        /// </summary>
        [Display(Name = "Album Performer")]
        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Performer { get; set; }

        /// <summary>
        /// Gets or sets the release year of the album.
        /// </summary>
        [Display(Name = "Album Release Year")]
        [Range(1900, 2022)]
        [Required]
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the albumee is a reissue or not..
        /// </summary>
        [Display(Name = "Album Reissueness")]
        [Required]
        public bool IsReissue { get; set; }
    }
}
