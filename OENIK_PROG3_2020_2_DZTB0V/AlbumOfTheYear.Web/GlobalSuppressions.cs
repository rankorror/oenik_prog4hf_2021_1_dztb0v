﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Reviewed", Scope = "member", Target = "~P:AlbumOfTheYear.Web.Models.AlbumListViewModel.ListOfAlbums")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Reviewed", Scope = "member", Target = "~P:AlbumOfTheYear.Web.Models.AlbumListViewModel.ListOfAlbums")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Gitstats>", Scope = "module")]
